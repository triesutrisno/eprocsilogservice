<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('/login', 'Api\UserController@login')->name('login');
Route::post('/logout', 'Api\UserController@logout')->name('logout');
Route::post('/register', 'Api\UserController@register');
Route::post('/registersupplier', 'Register\RegisterController@store');
Route::post('/dtpropinsi', 'Register\RegisterController@propinsi')->name('dtpropinsi');
Route::post('/dtkabupaten', 'Register\RegisterController@kabupaten');
Route::post('/dtkecamatan', 'Register\RegisterController@kecamatan');
Route::post('/active', 'Register\RegisterController@active');
Route::post('/displaysupplier', 'Supplier\SupplierController@index');
Route::post('/detailsupplier', 'Supplier\SupplierController@show');
Route::post('/verified', 'Supplier\SupplierController@verified');
Route::post('/notverified', 'Supplier\SupplierController@notverified');
Route::post('/datapribadi', 'Supplier\SupplierController@edit');
Route::post('/updatedatapribadi/{email}', 'Supplier\SupplierController@update');
Route::post('/dtKategori', 'Kategori\KategoriController@index');
Route::post('/dtCurrency', 'Currency\CurrencyController@index');
Route::post('/dtUnit', 'Unit\UnitController@index');

Route::post('/kategori', 'Kategori\KategoriController@index');
Route::post('/kategoripost', 'Kategori\KategoriController@store');
Route::post('/kategoriedit/{id}', 'Kategori\KategoriController@edit');
Route::post('/kategorieditpost', 'Kategori\KategoriController@update');
Route::post('/kategorihapus/{id}', 'Kategori\KategoriController@destroy');

Route::post('/kualifikasi', 'Kualifikasi\KualifikasiController@index');
Route::post('/kualifikasipost', 'Kualifikasi\KualifikasiController@store');
Route::post('/kualifikasiedit/{id}', 'Kualifikasi\KualifikasiController@edit');
Route::post('/kualifikasieditpost', 'Kualifikasi\KualifikasiController@update');
Route::post('/kualifikasihapus/{id}', 'Kualifikasi\KualifikasiController@destroy');

Route::post('/tender', 'Tender\TenderController@index');
Route::post('/tenderpost', 'Tender\TenderController@store');
Route::post('/tenderedit/{id}', 'Tender\TenderController@edit');
Route::post('/tendereditpost', 'Tender\TenderController@update');
Route::post('/tenderhapus/{id}', 'Tender\TenderController@destroy');
Route::post('/tendershow/{id}', 'Tender\TenderController@show');
Route::post('/tenderkirimemail', 'Tender\TenderController@kirimemail');
Route::post('/waktunego', 'Tender\TenderController@waktunego');
Route::post('/pemenang', 'Tender\TenderController@pemenang');
Route::post('/pemenangpost', 'Tender\TenderController@pemenangpost');
Route::post('/tenderdetail', 'Tender\TenderdetailController@index');
Route::post('/tenderdetailshow/{id}', 'Tender\TenderdetailController@show');
Route::post('/tenderdetailedit/{id}', 'Tender\TenderdetailController@edit');
Route::post('/tenderdetaileditpost', 'Tender\TenderdetailController@update');
Route::post('/tenderdetailnego/{id}', 'Tender\TenderdetailController@nego');
Route::post('/tenderdetailnegosiasipost', 'Tender\TenderdetailController@negopost');
Route::post('/cetaktender', 'Tender\TenderdetailController@cetaktender');


Route::post('/permintaanpembayaran', 'Permintaanpembayaran\PermintaanpembayaranController@index');
Route::post('/postpermintaanpembayaran/', 'Permintaanpembayaran\PermintaanpembayaranController@store');
Route::post('/permintaanpembayaranedit/{id}', 'Permintaanpembayaran\PermintaanpembayaranController@edit');
Route::post('/postpermintaanpembayaranedit/', 'Permintaanpembayaran\PermintaanpembayaranController@update');
Route::post('/permintaanpembayaranhapus/{id}', 'Permintaanpembayaran\PermintaanpembayaranController@destroy');
Route::post('/permintaanpembayaranshow/{id}', 'Permintaanpembayaran\PermintaanpembayaranController@show');
Route::post('/permintaanpembayarandetail/{id}', 'Permintaanpembayaran\PermintaanpembayaranController@detail');
Route::post('/permintaanpembayarancomplite/{id}', 'Permintaanpembayaran\PermintaanpembayaranController@complite');
Route::post('/permintaanpembayarannocomplite/{id}', 'Permintaanpembayaran\PermintaanpembayaranController@nocomplite');
Route::post('/permintaanpembayarannocomplitepost', 'Permintaanpembayaran\PermintaanpembayaranController@nocomplitepost');
Route::post('/permintaanpembayaranaddnopppost', 'Permintaanpembayaran\PermintaanpembayaranController@addnopppost');

Route::post('/prematchingtruk', 'Prematchingtruk\PrematchingtrukController@index');
Route::post('/prematchcreate', 'Prematchingtruk\PrematchingtrukController@create');
Route::post('/prematchstore', 'Prematchingtruk\PrematchingtrukController@store');
Route::post('/prematchhapus/{id}/{doc}/{dct}/{lnid}', 'Prematchingtruk\PrematchingtrukController@destroy');

Route::post('/monitoringdo', 'Monitoringdo\MonitoringdoController@index');
Route::post('/monitoringarmada', 'Monitoringarmada\MonitoringarmadaController@index');

Route::post('/unit', 'Unit\UnitController@index');
Route::post('/unitpost', 'Unit\UnitController@store');
Route::post('/unitedit/{id}', 'Unit\UnitController@edit');
Route::post('/uniteditpost', 'Unit\UnitController@update');
Route::post('/unithapus/{id}', 'Unit\UnitController@destroy');

Route::post('/nextnumber', 'Nextnumber\NextnumberController@index');
Route::post('/nextnumberpost', 'Nextnumber\NextnumberController@store');
Route::post('/nextnumberedit/{id}', 'Nextnumber\NextnumberController@edit');
Route::post('/nextnumbereditpost', 'Nextnumber\NextnumberController@update');
Route::post('/nextnumberhapus/{id}', 'Nextnumber\NextnumberController@destroy');


Route::get('/berita_acara', 'BeritaAcaraController@index');
Route::post('/berita_acara', 'BeritaAcaraController@store');
Route::get('/berita_acara_show', 'BeritaAcaraController@show');
Route::post('/berita_acara_approve', 'BeritaAcaraController@approve');
Route::post('/berita_acara_reject', 'BeritaAcaraController@reject');
Route::post('/berita_acara_delete', 'BeritaAcaraController@delete');
Route::get('/berita_acara_print', 'BeritaAcaraController@print');
Route::get('/berita_acara_excel_spj', 'BeritaAcaraController@excelSPJ');
Route::post('/berita_acara_generate_no_pp', 'BeritaAcaraController@generateNoPP');
Route::post('/berita_acara_set_inputted_sap', 'BeritaAcaraController@setInputtedSAP');
Route::post('/berita_acara_generate_surat_klaim', 'BeritaAcaraController@generateSuratKlaim');
Route::get('/berita_acara_print_surat_klaim', 'BeritaAcaraController@printSuratKlaim');
Route::get('/berita_acara_approve_surat_klaim', 'BeritaAcaraController@approveSuratKlaim');
Route::get('/berita_acara_reject_surat_klaim', 'BeritaAcaraController@rejectSuratKlaim');


Route::get('/berita_acara_sbi', 'BeritaAcaraSBIController@index');
Route::post('/berita_acara_sbi', 'BeritaAcaraSBIController@store');
Route::get('/berita_acara_sbi_show', 'BeritaAcaraSBIController@show');
Route::post('/berita_acara_sbi_approve', 'BeritaAcaraSBIController@approve');
Route::post('/berita_acara_sbi_reject', 'BeritaAcaraSBIController@reject');
Route::post('/berita_acara_sbi_delete', 'BeritaAcaraSBIController@delete');
Route::get('/berita_acara_sbi_print', 'BeritaAcaraSBIController@print');
Route::get('/berita_acara_sbi_excel_spj', 'BeritaAcaraSBIController@excelSPJ');
Route::post('/berita_acara_sbi_generate_no_pp', 'BeritaAcaraSBIController@generateNoPP');
Route::post('/berita_acara_sbi_set_inputted_sap', 'BeritaAcaraSBIController@setInputtedSAP');
Route::post('/berita_acara_sbi_generate_surat_klaim', 'BeritaAcaraSBIController@generateSuratKlaim');
Route::get('/berita_acara_sbi_print_surat_klaim', 'BeritaAcaraSBIController@printSuratKlaim');
Route::get('/berita_acara_sbi_approve_surat_klaim', 'BeritaAcaraSBIController@approveSuratKlaim');
Route::get('/berita_acara_sbi_reject_surat_klaim', 'BeritaAcaraSBIController@rejectSuratKlaim');

Route::get('/berita_acara_sp_zak', 'BeritaAcaraSPZakController@index');
Route::post('/berita_acara_sp_zak', 'BeritaAcaraSPZakController@store');
Route::get('/berita_acara_sp_zak_show', 'BeritaAcaraSPZakController@show');
Route::post('/berita_acara_sp_zak_approve', 'BeritaAcaraSPZakController@approve');
Route::post('/berita_acara_sp_zak_reject', 'BeritaAcaraSPZakController@reject');
Route::post('/berita_acara_sp_zak_delete', 'BeritaAcaraSPZakController@delete');
Route::get('/berita_acara_sp_zak_print', 'BeritaAcaraSPZakController@print');
Route::get('/berita_acara_sp_zak_excel_spj', 'BeritaAcaraSPZakController@excelSPJ');
Route::post('/berita_acara_sp_zak_generate_no_pp', 'BeritaAcaraSPZakController@generateNoPP');
Route::post('/berita_acara_sp_zak_set_inputted_sap', 'BeritaAcaraSPZakController@setInputtedSAP');
Route::post('/berita_acara_sp_zak_generate_surat_klaim', 'BeritaAcaraSPZakController@generateSuratKlaim');
Route::get('/berita_acara_sp_zak_print_surat_klaim', 'BeritaAcaraSPZakController@printSuratKlaim');
Route::get('/berita_acara_sp_zak_approve_surat_klaim', 'BeritaAcaraSPZakController@approveSuratKlaim');
Route::get('/berita_acara_sp_zak_reject_surat_klaim', 'BeritaAcaraSPZakController@rejectSuratKlaim');


Route::get('/berita_acara_sp_curah', 'BeritaAcaraSPCurahController@index');
Route::post('/berita_acara_sp_curah', 'BeritaAcaraSPCurahController@store');
Route::get('/berita_acara_sp_curah_show', 'BeritaAcaraSPCurahController@show');
Route::post('/berita_acara_sp_curah_approve', 'BeritaAcaraSPCurahController@approve');
Route::post('/berita_acara_sp_curah_reject', 'BeritaAcaraSPCurahController@reject');
Route::post('/berita_acara_sp_curah_delete', 'BeritaAcaraSPCurahController@delete');
Route::get('/berita_acara_sp_curah_print', 'BeritaAcaraSPCurahController@print');
Route::get('/berita_acara_sp_curah_excel_spj', 'BeritaAcaraSPCurahController@excelSPJ');
Route::post('/berita_acara_sp_curah_generate_no_pp', 'BeritaAcaraSPCurahController@generateNoPP');
Route::post('/berita_acara_sp_curah_set_inputted_sap', 'BeritaAcaraSPCurahController@setInputtedSAP');
Route::post('/berita_acara_sp_curah_generate_surat_klaim', 'BeritaAcaraSPCurahController@generateSuratKlaim');
Route::get('/berita_acara_sp_curah_print_surat_klaim', 'BeritaAcaraSPCurahController@printSuratKlaim');
Route::get('/berita_acara_sp_curah_approve_surat_klaim', 'BeritaAcaraSPCurahController@approveSuratKlaim');
Route::get('/berita_acara_sp_curah_reject_surat_klaim', 'BeritaAcaraSPCurahController@rejectSuratKlaim');


Route::get('/invoice_monitor', 'InvoiceMonitorController@index');
Route::get('/invoice_monitor-all', 'InvoiceMonitorController@indexAll');
Route::post('/invoice_monitor-upload', 'InvoiceMonitorController@upload');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', 'Api\UserController@details');
});
