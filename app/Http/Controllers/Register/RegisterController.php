<?php

namespace App\Http\Controllers\Register;

use App\Http\Controllers\Controller;
use App\Http\Models\Supplier\Supplier;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Http\Models\Propinsi\Propinsi;
use App\Http\Models\Kabupaten\Kabupaten;
use App\Http\Models\Kecamatan\Kecamatan;
use App\Http\Models\Kategori\Kategori;
use App\Http\Models\Kualifikasi\Kualifikasi;
use App\Http\Models\Unit\Unit;
use App\Http\Models\Suppunit\Suppunit;
use App\Http\Models\Sendmail\Sendmail;

class RegisterController extends Controller
{
    public $successStatus = 200;
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $validator = Validator::make($request->all(), [
                'supNama' => 'required',
                'supEmail' => 'required|email',
                'supNPWP' => 'required',
                'supSetuju' => 'required',
        ]);        

        if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        $expEmail = explode("@",$request->supEmail);
        if(strtolower($expEmail[1])== "yahoo.com" || strtolower($expEmail[1]) == "yahoo.co.id")
        {
            return response()->json(["kode"=>"90", "pesan"=>"Mohon untuk menggunakan domain lainnya !"], $this->successStatus);
        }

        if (Supplier::where('supEmail', '=', $request->supEmail)->where('supStatus', '!=', '4')->whereNull('deleted_at')->doesntExist()) { // Cek data apakah email sudah ada atau belum di database           
            if (Supplier::where('supNPWP', '=', $request->supNPWP)->where('supStatus', '!=', '4')->whereNull('deleted_at')->doesntExist()) { // Cek data apakah NPWP sudah ada atau belum di database 
                /** Insert ke tabel vendor **/
                $request->request->add(['supStatus'=>'1']);
                $request->request->add(['has'=>$request->password]);
                $supp = Supplier::create($request->all());      
                /** Insert ke tabel Suppunit **/
                if($request->unit<>NULL){
                    foreach($request->unit as $key => $val){
                        $suppUnit = new Suppunit;
                        $suppUnit->supId = $supp->supId;
                        $suppUnit->unitId = $key;
                        $suppUnit->save();
                    }
                }
                
                /** Insert ke tbale User **/
                #$user = new User;
                #$user->name = $request->supNama;
                #$user->email = $request->supEmail;
                #$user->role = "supplier";
                #$user->status = "1";
                #$user->password = bcrypt($request->password);
                #$user->save();
                
                /** Send Email **/
                $isiEmail="<html>";
                $isiEmail.="<html>";
                $isiEmail.="<body>";
                $isiEmail.="<h3>Terima Kasih !</h3><br />";                
                $isiEmail.="Selamat anda telah mendaftarkan email ".$request->supEmail." sebagai calon supplier SILOG Group. <br />";
                $isiEmail.= "Silakan melakukan aktifasi email anda dengan klik link dibawah ini <br /><br />";
                $isiEmail.="<a href='https://eproc.silog.co.id/active/".$request->supEmail."'>Verify Email</a><br />"; 
                $isiEmail.="Atau buka URL : 'https://eproc.silog.co.id/active/".$request->supEmail."<br /><br />"; 
                $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
                $isiEmail.= "</body>";
                $isiEmail.="</html>";
                
                $email = new Sendmail;
                $email->Sender = "sistem@silog.co.id";
                $email->Tanggal = date("Y-m-d H:i:s");
                $email->Recipients = $request->supEmail;
                $email->CC = ""; 
                $email->SubjectEmail = "Aktivasi Akun Eproc Silog";
                // $email->IsiEmail = addslashes($isiEmail);
                $email->IsiEmail = $isiEmail;
                $email->Stat = "outbox";
                $email->email_password = "Veteran1974!@Gs";
                $email->ContentEmail = "0";
                $email->sistem = "eprocAktifasi";
                $email->save();
                
                return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disampan !'], $this->successStatus);
            }else{
                return response()->json(['kode'=>'90', 'pesan'=>'Data NPWP sudah ada !'], $this->successStatus);
            }
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data Email sudah ada !'], $this->successStatus);
        }
    }
    
    public function propinsi(){
        $dtProp = Propinsi::select('mpropId','mpropNama')
                            ->where(['mpropStatus'=>'1'])->get();
        
        $dtKat = Kategori::select('mkatId','mkatNama')
                            ->where(['mkatStatus'=>'1'])->get();
        
        $dtKual = Kualifikasi::select('mkualId','mkualNama')
                            ->where(['mkualStatus'=>'1'])->get();
        
        $dtUnit = Unit::select('unitId', 'unit', 'keterangan')
                            ->where(['status'=>'1'])->get();
        
        $jml = count($dtProp);
        if($jml>0){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil ditemukan !', 'dtProp'=>$dtProp, 'dtKat'=>$dtKat, 'dtKual'=>$dtKual, 'dtUnit'=>$dtUnit], $this->successStatus);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak  ditemukan !', 'dtProp'=>"", 'dtKat'=>"", 'dtKual'=>"", 'dtUnit'=>""], $this->successStatus);
        }
    }
    
    public function kabupaten(Request $request){
        $dtKab = Kabupaten::select('mkabId','mkabNama','mpropId')
                            ->where(['mkabStatus'=>'1', 'mpropId'=>$request->value])->get();
        $jml = count($dtKab);
        if($jml>0){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil ditemukan !', 'dtKab'=>$dtKab], $this->successStatus);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak  ditemukan !', 'dtKab'=>""], $this->successStatus);
        }
    }
    
    public function kecamatan(Request $request){
        $dtKec = Kecamatan::select('mkecId','mkecNama','mkabId')
                            ->where(['mkecStatus'=>'1', 'mkabId'=>$request->value])->get();
        $jml = count($dtKec);
        if($jml>0){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil ditemukan !', 'dtKec'=>$dtKec], $this->successStatus);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak  ditemukan !', 'dtKec'=>""], $this->successStatus);
        }
    }
    
    public function active(Request $request){
        $supp = Supplier::where(['supEmail'=>$request->email,'supStatus'=>'1'])
                ->get();
        #dd($supp[0]);
        if(isset($supp[0]['supId'])){ 
            if($supp[0]['supStatus']=='1'){
                $updSupp = Supplier::where('supId', $supp[0]['supId'])
                        ->update(['supEmailStatus'=>'1', 'supStatus'=>'2']);
                if($updSupp){
                    /** Send Email **/
                    $isiEmail="<html>";
                    $isiEmail.="<html>";
                    $isiEmail.="<body>";
                    $isiEmail.="<h3>Info Pendaftaran Supplier Baru !</h3><br />";                
                    $isiEmail.="Saat ini ada supplier baru yang mendaftar untuk menjadi calon supplier di PT Semen Indonesia Group. <br />";
                    $isiEmail.= "Silakan lakukan verifikasi supplier dengan cara mengunjungi link dibawah ini <br />";
                    $isiEmail.="eproc.silog.co.id <br />"; 
                    $isiEmail.="Pakai user dan password anda untuk masuk kedalam sistem <br />"; 
                    $isiEmail.="Terima Kasih <br /><br /><br /><br />"; 
                    $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
                    $isiEmail.= "</body>";
                    $isiEmail.="</html>";

                    $email = new Sendmail;
                    $email->Sender = "sistem@silog.co.id";
                    $email->Tanggal = date("Y-m-d H:i:s");
                    $email->Recipients = "pengadaan@silog.co.id";
                    $email->CC = "triesutrisno@silog.co.id"; 
                    $email->SubjectEmail = "Informasi Supplier Baru";
                    $email->IsiEmail = addslashes($isiEmail);
                    $email->Stat = "outbox";
                    $email->email_password = "Veteran1974!@Gs";
                    $email->ContentEmail = "0";
                    $email->sistem = "eprocInfo";
                    $email->save();
                    return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil diupdate !'], 200);
                }else{
                    return response()->json(['kode'=>'90', 'pesan'=>'Data tidak berhasil diupdate, '], 200);
                }
            }else{
                return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa verfikasi, '], 200);
            }
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak ditemukan, '], 200);
        }
    }
}
