<?php

namespace App\Http\Controllers\Monitoringarmada;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Models\Supplier\Supplier;


class MonitoringarmadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nopin = $request->param["nopin"] != NULL ? $request->param["nopin"] : "";
        $nopol = $request->param["nopol"] != NULL ? $request->param["nopol"] : "";
        $driver = $request->param["driver"] != NULL ? $request->param["driver"] : "";
        $pemilikArmada = $request->param["pemilikArmada"] != NULL ? $request->param["pemilikArmada"] : "";
        $status = $request->param["status"] != NULL ? $request->param["status"] : "";
        $statusTruk = $request->param["statusTruk"] != NULL ? $request->param["statusTruk"] : "";
        $statusMuatan = $request->param["statusMuatan"] != NULL ? $request->param["statusMuatan"] : "";

        #return $request->all();  
        
        $dtSupp = Supplier::where(['supEmail'=>$request->email])->get();
        #return $dtSupp;

        $data = DB::connection('PRODDTA')
            ->table('VW_TM_DO_AKTIF_TRUK')
            ->select(
                'NOPOL',
                'NOPIN',
                'MOT',
                'JENISTRUK',
                'PEMILIKTRUK',
                'NAMAPEMILIKTRUK',
                'KODEDRIVER',
                'NAMADRIVER',
                'NIKDRIVER',
                'KODEVENDORDRV',
                'NAMAVENDORDRV',
                'ALAMAT',
                'KECAMATAN',
                'KABUPATEN',
                'PROVINSI',
                'STATUSARMADA',
                'DURASI',
                'DURASI2',
                'STS_PERJ',
                'STATUSMUATAN',
                'DO_AKTIF',
                'DO_TY',
                'DO_CO',
                'DO_LINE'
            )
            ->where(['PEMILIKTRUK'=>$dtSupp[0]['kodeJDE']])
            ->when($nopin, function ($query, $nopin) {
                    return $query->where(DB::raw("TRIM(NOPIN)"), $nopin);
                })         
            ->when($nopol, function ($query, $nopol) {
                    return $query->where(DB::raw("TRIM(NOPOL)"), $nopol);
                })
            ->when($driver, function ($query, $driver) {
                    return $query->where('NAMADRIVER', 'like', '%'.$driver.'%');
                })
            ->when($pemilikArmada, function ($query, $pemilikArmada) {
                    return $query->where('NAMAPEMILIKTRUK', 'like', '%'.$pemilikArmada.'%');
                })            
            ->when($status, function ($query, $status) {
                    return $query->where('STS_PERJ', $status);
                })        
            ->when($statusTruk, function ($query, $statusTruk) {
                    return $query->where("STATUSARMADA", $statusTruk);
                })         
            ->when($statusMuatan, function ($query, $statusMuatan) {
                    return $query->where('STATUSMUATAN', $statusMuatan);
                })
            ->orderBy('DURASI2', 'desc')
            ->get();

        return ['data'=>$data];
    }
}
