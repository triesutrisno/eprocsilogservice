<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\BeritaAcaraSBI;
use App\Http\Models\BeritaAcaraSpjSBI;
use App\Http\Resources\BeritaAcaraResource;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class BeritaAcaraSBIController extends Controller
{
    public function index(Request $request)
    {
        // dd($request);
        $per_page = $request->get('per_page', 10);
        $orderBy = $request->get('order_by', "id_berita_acara");
        $orderType = $request->get('order_type', "desc");

        $no_berita_acara = $request->get("no_berita_acara");
        $nama_supplier = $request->get("nama_supplier");
        $no_kontrak_perjanjian = $request->get("no_kontrak_perjanjian");
        $status = $request->get("approve_status");
        $tgl_approve = $request->get("tgl_approve");
        $status_barang = $request->get("status_barang");
        $no_spj = $request->get("no_spj");
        $no_spj_sbi = $request->get("no_spj_sbi");
        $no_pp = $request->get("no_pp");
        $status_input_sap = $request->get("status_input_sap");
        $status_generate_klaim = $request->get("status_generate_klaim");
        $status_approve_klaim = $request->get("status_approve_klaim");

        $id_user = $request->get("id_user");

        // dd($status);

        $user = User::find($id_user);
        // dd($user);
        // $data = BeritaAcaraSBI::query();
        $data = BeritaAcaraSBI::withCount(['spj', 'spj_approved' => function (Builder $query) {
            $query->where('approve_status', 1);
        }]);

        $data->when($user->role != 'administrator', function ($query) use ($user) {
            return $query->where('id_user', $user->id);
        })
            // ->when($nama_supplier, function ($query, $nama_supplier) {
            //     return $query->whereHas('supplier', function ($q) use ($nama_supplier) {
            //         $q->where('supNama', 'LIKE', '%' . $nama_supplier . '%');
            //     });
            // })
            ->when($nama_supplier, function ($query, $nama_supplier) {
                return $query->where('nama_supplier', 'LIKE', '%' . $nama_supplier . '%');
            })
            ->when($no_berita_acara, function ($query, $no_berita_acara) {
                return $query->where('no_berita_acara', 'LIKE', '%' . $no_berita_acara . '%');
            })
            ->when($no_kontrak_perjanjian, function ($query, $no_kontrak_perjanjian) {
                return $query->where('no_kontrak_perjanjian', 'LIKE', '%' . $no_kontrak_perjanjian . '%');
            })
            ->when($status_barang, function ($query, $status_barang) {
                return $query->where('status_barang',  $status_barang);
            })

            ->when($no_spj, function ($query, $no_spj) {
                return $query->whereHas('spj', function ($q) use ($no_spj) {
                    $q->where('no_spj', 'LIKE', '%' . $no_spj . '%');
                });
            })

            ->when($no_spj_sbi, function ($query, $no_spj_sbi) {
                return $query->whereHas('spj', function ($q) use ($no_spj_sbi) {
                    $q->where('no_spj_sbi', 'LIKE', '%' . $no_spj_sbi . '%');
                });
            })

            ->when($no_pp, function ($query, $no_pp) {
                return $query->where('no_pp', 'LIKE', '%' . $no_pp . '%');
            });
        if (isset($status)) {
            $data->where('approve_status', $status);
        };

        if (request()->filled('tgl_approve')) {
            $param = explode(" - ", $tgl_approve);
            $start = Carbon::parse($param[0]);
            $end = Carbon::parse($param[1])->addDay();
            // dd($start);
            $data->whereBetween('approved_at', [$start, $end]);
        }


        if (isset($status_input_sap)) {
            if ($status_input_sap == 1) {
                $data->whereNotNull('no_pp')->where('is_inputted_sap', 1);
            } elseif ($status_input_sap == 0) {
                $data->whereNotNull('no_pp')->where('is_inputted_sap', '!=', 1);
            }
        }

        if (isset($status_generate_klaim)) {
            if ($status_generate_klaim == 1) {
                $data->where('status_barang', 'KLAIM')
                    ->where('approve_status', 1)
                    ->whereNotNull('no_surat_klaim');
            } elseif ($status_generate_klaim == 0) {
                $data->where('status_barang', 'KLAIM')
                    ->where('approve_status', 1)
                    ->whereNull('no_surat_klaim');
            }
        }

        if (isset($status_approve_klaim)) {
            if ($status_approve_klaim == 1) {
                $data
                    ->whereNotNull('no_surat_klaim')
                    ->where('surat_klaim_approve_status', 1);
            } elseif ($status_approve_klaim == 0) {
                $data
                    ->whereNotNull('no_surat_klaim')
                    ->where('surat_klaim_approve_status', '!=', 1);
            }
        }

        // dd($data->toSql());

        $data = $data->orderBy($orderBy, $orderType)->paginate($per_page);
        return BeritaAcaraResource::collection($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        // $request->validate([
        //     'id_user' => 'required',
        //     'no_kontrak_perjanjian' => 'required',
        //     'jenis_pelanggan' => 'required',
        //     'berita_acara_spj' => 'required|mimes:xls,xlsx',
        // ]);

        // dd($request->file('berita_acara_spj'));

        $message = "";
        $status = "";
        try {
            //code...

            DB::transaction(function () use ($request, &$message, &$status) {
                $beritaAcara = new BeritaAcaraSBI();
                $beritaAcara->no_kontrak_perjanjian = $request->get('no_kontrak_perjanjian');
                $beritaAcara->jenis_pelanggan = $request->get('jenis_pelanggan');
                $beritaAcara->nama_pic_supplier = $request->get('nama_pic_supplier');
                $beritaAcara->periode_spj_awal = $request->get('periode_spj_awal');
                $beritaAcara->periode_spj_akhir = $request->get('periode_spj_akhir');
                $beritaAcara->jenis_plat = $request->get('jenis_plat');
                $beritaAcara->plant = $request->get('plant');
                $beritaAcara->status_barang = $request->get('status_barang');

                $user = User::find($request->get('id_user'));
                $beritaAcara->id_user = $user->id;
                $beritaAcara->id_supplier = $user->supplier->supId;
                $beritaAcara->nama_supplier = $user->supplier->supNama;

                $beritaAcara->approve_status = 0;
                $beritaAcara->save();

                $beritaAcara->no_berita_acara = 'BA-SBI-' . str_pad($beritaAcara->id_berita_acara, 8, '0', STR_PAD_LEFT);
                $beritaAcara->save();


                $beritaAcaraSpjList = (new FastExcel)->import(
                    $request->file('berita_acara_spj'),
                    function ($line) use ($beritaAcara) {

                        // no spj kosong, return
                        if (empty($line['NO SPJ']) && empty($line['NO SPJ SBI'])) {
                            return;
                        }

                        // check no spj SI sudah ada
                        if (!empty($line['NO SPJ'])) {
                            $dataCount = BeritaAcaraSpjSBI::where('no_spj', strval($line['NO SPJ']))
                                ->where('approve_status', '!=', '-1')
                                ->count();

                            if ($dataCount > 0) {
                                throw new Exception("Gagal Upload. No SPJ SI " . $line['NO SPJ'] . " sudah ada");
                            }
                        }

                        // check no spj SBI sudah ada
                        if (!empty($line['NO SPJ SBI'])) {
                            $dataCount = BeritaAcaraSpjSBI::where('no_spj_sbi', strval($line['NO SPJ SBI']))
                                ->where('approve_status', '!=', '-1')
                                ->count();

                            if ($dataCount > 0) {
                                throw new Exception("Gagal Upload. No SPJ SBI " . $line['NO SPJ SBI'] . " sudah ada");
                            }
                        }


                        return BeritaAcaraSpjSBI::create([
                            'id_berita_acara' => $beritaAcara->id_berita_acara,
                            'no_spj' => $line['NO SPJ'],
                            'tgl_spj' => $line['TGL SPJ'],
                            'tujuan_bongkar' => $line['TUJUAN BONGKAR'],
                            'no_polisi' => $line['NO POLISI'],
                            'qty_40' => $line['QTY 40'],
                            'qty_50' => $line['QTY 50'],
                            'qty_semen_jumbo_bag' => isset($line['QTY SEMEN JUMBO BAG']) ? $line['QTY SEMEN JUMBO BAG'] : 0,
                            'klaim_kantong' => $line['KLAIM KANTONG'],
                            'klaim_semen' => $line['KLAIM SEMEN'],
                            'approve_status' => 0,
                            'no_spj_sbi' => $line['NO SPJ SBI']
                        ]);
                    }
                );

                $status = "success";
                $message = "Berhasil Menyimpan";
            });
        } catch (\Exception $e) {
            $status = "error";
            $message = "Error!! " . $e->getMessage();
        }

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function show(Request $request)
    {
        $id_berita_acara = $request->get('id_berita_acara');

        $beritaAcara = BeritaAcaraSBI::with('spj')->find($id_berita_acara);

        return $beritaAcara;
    }

    public function approve(Request $request)
    {
        $message = "";
        $status = "";

        DB::transaction(function () use ($request, &$message, &$status) {

            $beritaAcara = BeritaAcaraSBI::find($request->get('id_berita_acara'));
            $ids = json_decode($request->get('ids'));
            $user = User::find($request->get('id_user'));

            if ($user->role != 'administrator') {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($beritaAcara->approve_status != 0) {
                $status = "error";
                $message = "Sudah diproses Approval sebelumnya!";
                return;
            }

            if (sizeof($ids) <= 0) {
                $status = "error";
                $message = "Tidak ada data yang dipilih !";
                return;
            }

            $beritaAcara->approve_status = 1;
            $beritaAcara->approved_by = $request->get('id_user');
            $beritaAcara->approved_at = Carbon::now();
            $beritaAcara->approver_name = $user->name;
            $beritaAcara->save();

            // approve all ids sent
            BeritaAcaraSpjSBI::whereIn('id_berita_acara_spj', $ids)->update([
                'approve_status' => 1,
                'approved_by' => $request->get('id_user'),
                'approved_at' => Carbon::now(),
            ]);

            // reject others not approved
            BeritaAcaraSpjSBI::where('id_berita_acara', $beritaAcara->id_berita_acara)
                ->where('approve_status', 0)
                ->update([
                    'approve_status' => -1,
                    'approved_by' => $request->get('id_user'),
                    'approved_at' => Carbon::now(),
                ]);

            $status = "success";
            $message = "Berhasil Approve";
        });

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function reject(Request $request)
    {
        $message = "";
        $status = "";

        DB::transaction(function () use ($request, &$message, &$status) {

            $beritaAcara = BeritaAcaraSBI::find($request->get('id_berita_acara'));
            $user = User::find($request->get('id_user'));

            if ($user->role != 'administrator') {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($beritaAcara->approve_status != 0) {
                $status = "error";
                $message = "Sudah diproses Approval sebelumnya!";
                return;
            }

            $beritaAcara->approve_status = -1;
            $beritaAcara->approved_by = $request->get('id_user');
            $beritaAcara->approved_at = Carbon::now();
            $beritaAcara->approver_name = $user->name;
            $beritaAcara->save();

            // reject spj
            BeritaAcaraSpjSBI::where('id_berita_acara', $beritaAcara->id_berita_acara)
                ->where('approve_status', 0)
                ->update([
                    'approve_status' => -1,
                    'approved_by' => $request->get('id_user'),
                    'approved_at' => Carbon::now(),
                ]);

            $status = "success";
            $message = "Berhasil Reject";
        });

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function delete(Request $request)
    {
        $message = "";
        $status = "";

        DB::transaction(function () use ($request, &$message, &$status) {

            $beritaAcara = BeritaAcaraSBI::find($request->get('id_berita_acara'));
            $user = User::find($request->get('id_user'));

            if ($beritaAcara->id_user != $user->id) {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($beritaAcara->approve_status != 0) {
                $status = "error";
                $message = "Sudah diproses Approval!";
                return;
            }

            $beritaAcara->delete();
            $beritaAcara->spj()->each(function ($spj) {
                $spj->delete();
            });

            $status = "success";
            $message = "Berhasil Delete";
        });

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function print(Request $request)
    {
        $id_berita_acara = $request->get('id_berita_acara');

        $beritaAcara = BeritaAcaraSBI::with('spj')->find($id_berita_acara);

        return $beritaAcara;
    }

    public function excelSPJ(Request $request)
    {
        $id_berita_acara = $request->get('id_berita_acara');

        $beritaAcara = BeritaAcaraSBI::with('spj')->find($id_berita_acara);

        $spjList = BeritaAcaraSpjSBI::where('id_berita_acara', $id_berita_acara)->orderBy('id_berita_acara_spj')->get();

        return (new FastExcel($spjList))->download('BA SPJ List.xlsx', function ($spj) {
            return [
                'NO SPJ' => $spj->no_spj,
                'NO SPJ SBI' => $spj->no_spj_sbi,
                'TGL SPJ' => Carbon::parse($spj->tgl_spj)->format('m/d/Y'),
                'TUJUAN BONGKAR' => $spj->tujuan_bongkar,
                'NO POLISI' => $spj->no_polisi,
                'QTY 40' => $spj->qty_40,
                'QTY 50' => $spj->qty_50,
                'QTY JUMBO BAG' => $spj->qty_semen_jumbo_bag,
                'KLAIM KANTONG' => $spj->klaim_kantong,
                'KLAIM SEMEN' => $spj->klaim_semen,
                'KODE CARTER' => $spj->beritaAcara->supplier->kodeJDE,
                'NAMA MITRA CARTER' => $spj->beritaAcara->supplier->supNama,
                'STATUS' => $spj->approve_status == 1 ? 'APPROVED' : ($spj->approve_status == -1 ? 'REJECTED' : 'WAITING')

            ];
        });
    }

    public function generateNoPP(Request $request)
    {
        $message = "";
        $status = "";

        DB::transaction(function () use ($request, &$message, &$status) {

            $beritaAcara = BeritaAcaraSBI::find($request->get('id_berita_acara'));
            $user = User::find($request->get('id_user'));

            if ($user->role != 'administrator') {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($beritaAcara->approve_status != 1) {
                $status = "error";
                $message = "Berita Acara Belum/Tidak Approved";
                return;
            }

            if ($beritaAcara->no_pp != null) {
                $status = "error";
                $message = "Sudah memiliki No PP";
                return;
            }

            $beritaAcara->no_pp = 'EPP-SBI-' . date('m') . date('Y') . '-' . str_pad($beritaAcara->id_berita_acara, 8, '0', STR_PAD_LEFT);
            $beritaAcara->save();

            $status = "success";
            $message = "Berhasil Generate No PP";
        });

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function setInputtedSAP(Request $request)
    {
        $message = "";
        $status = "";

        DB::transaction(function () use ($request, &$message, &$status) {

            $beritaAcara = BeritaAcaraSBI::find($request->get('id_berita_acara'));
            $user = User::find($request->get('id_user'));

            if ($user->role != 'administrator') {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($beritaAcara->approve_status != 1) {
                $status = "error";
                $message = "Berita Acara Belum/Tidak Approved";
                return;
            }

            if ($beritaAcara->no_pp == null) {
                $status = "error";
                $message = "Belum memiliki No PP";
                return;
            }

            if ($beritaAcara->is_inputted_sap == 1) {
                $status = "error";
                $message = "Sudah set input sap sebelumnya";
                return;
            }

            $beritaAcara->is_inputted_sap = 1;
            $beritaAcara->save();

            $status = "success";
            $message = "Berhasil Set Inputted SAP";
        });

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function generateSuratKlaim(Request $request)
    {
        $message = "";
        $status = "";

        DB::transaction(function () use ($request, &$message, &$status) {

            $beritaAcara = BeritaAcaraSBI::find($request->get('id_berita_acara'));
            $hargaKantong = $request->get('surat_klaim_harga_kantong');
            $hargaSemen = $request->get('surat_klaim_harga_semen');
            $keterangan = $request->get('surat_klaim_keterangan');
            $user = User::find($request->get('id_user'));

            if ($user->role != 'administrator') {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($beritaAcara->approve_status != 1) {
                $status = "error";
                $message = "Berita Acara Belum/Tidak Approved";
                return;
            }

            if (!empty($beritaAcara->no_surat_klaim)) {
                $status = "error";
                $message = "Sudah ada No Surat Klaim";
                return;
            }

            if ($beritaAcara->status_barang != 'KLAIM') {
                $status = "error";
                $message = "Bukan BA jenis KLAIM";
                return;
            }

            $beritaAcara->no_surat_klaim = 'KLMSBI' . str_pad($beritaAcara->id_berita_acara, 8, '0', STR_PAD_LEFT) . "/" . date('m') . date('Y');
            $beritaAcara->surat_klaim_approve_status = 0;
            $beritaAcara->surat_klaim_harga_kantong = $hargaKantong;
            $beritaAcara->surat_klaim_harga_semen = $hargaSemen;
            $beritaAcara->surat_klaim_keterangan = $keterangan;
            $beritaAcara->save();

            $status = "success";
            $message = "Berhasil Generate Surat Klaim";
        });

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function printSuratKlaim(Request $request)
    {
        $id_berita_acara = $request->get('id_berita_acara');

        $beritaAcara = BeritaAcaraSBI::with('spj')->find($id_berita_acara);
        $data['id_berita_acara'] = $beritaAcara->id_berita_acara;
        $data['no_berita_acara'] = $beritaAcara->no_berita_acara;
        $data['nama_supplier'] = $beritaAcara->nama_supplier;
        $data['no_surat_klaim'] = $beritaAcara->no_surat_klaim;
        $data['tgl_ba'] = $beritaAcara->created_at;
        $data['klaim_kantong'] = $beritaAcara->spj->where('approve_status', '=', 1)->sum('klaim_kantong');
        $data['klaim_semen'] = $beritaAcara->spj->where('approve_status', '=', 1)->sum('klaim_semen');
        $data['harga_kantong'] = $beritaAcara->surat_klaim_harga_kantong;
        $data['harga_semen'] = $beritaAcara->surat_klaim_harga_semen;
        $data['keterangan'] = $beritaAcara->surat_klaim_keterangan;
        $data['surat_klaim_approve_status'] = $beritaAcara->surat_klaim_approve_status;
        $data['surat_klaim_approved_by'] = $beritaAcara->surat_klaim_approved_by;
        $data['surat_klaim_approved_at'] = $beritaAcara->surat_klaim_approved_at;
        $data['approver_name'] = $beritaAcara->suratKlaimApprovedByUser->name ?? "";

        // dd($data);

        return $data;
    }

    public function approveSuratKlaim(Request $request)
    {
        $message = "";
        $status = "";

        DB::transaction(function () use ($request, &$message, &$status) {

            $beritaAcara = BeritaAcaraSBI::find($request->get('id_berita_acara'));
            $user = User::find($request->get('id_user'));

            if ($user->role != 'administrator') {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($user->is_ka_billing != 1) {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($beritaAcara->surat_klaim_approve_status != 0) {
                $status = "error";
                $message = "Sudah diproses Approval sebelumnya!";
                return;
            }

            $beritaAcara->surat_klaim_approve_status = 1;
            $beritaAcara->surat_klaim_approved_by = $request->get('id_user');
            $beritaAcara->surat_klaim_approved_at = Carbon::now();
            $beritaAcara->save();

            $status = "success";
            $message = "Berhasil Approve Surat Klaim";
        });

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function rejectSuratKlaim(Request $request)
    {
        $message = "";
        $status = "";

        DB::transaction(function () use ($request, &$message, &$status) {

            $beritaAcara = BeritaAcaraSBI::find($request->get('id_berita_acara'));
            $user = User::find($request->get('id_user'));

            if ($user->role != 'administrator') {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($user->is_ka_billing != 1) {
                $status = "error";
                $message = "Tidak Berhak Melakukan";
                return;
            }

            if ($beritaAcara->surat_klaim_approve_status != 0) {
                $status = "error";
                $message = "Sudah diproses Approval sebelumnya!";
                return;
            }

            $beritaAcara->surat_klaim_approve_status = -1;
            $beritaAcara->no_surat_klaim = null;
            $beritaAcara->surat_klaim_harga_kantong = null;
            $beritaAcara->surat_klaim_harga_semen = null;
            $beritaAcara->save();

            $status = "success";
            $message = "Berhasil Reject Surat Klaim";
        });

        return response(['status' => $status, 'message' => $message], 200);
    }
}
