<?php

namespace App\Http\Controllers\Kualifikasi;

use App\Http\Controllers\Controller;
use App\Http\Models\Kualifikasi\Kualifikasi;
use Illuminate\Http\Request;

class KualifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kualifikasis = Kualifikasi::all();
        return $kualifikasis;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Kualifikasi::where(['mkualNama'=>$request->mkualNama, 'mkualStatus'=>'1', 'deleted_at'=>NULL])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Kualifikasi::create($request->all());
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Kualifikasi\Kualifikasi  $kualifikasi
     * @return \Illuminate\Http\Response
     */
    public function show(Kualifikasi $kualifikasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Kualifikasi\Kualifikasi  $kualifikasi
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kualifikasi = Kualifikasi::where(['mkualId'=>$id])->get();
        return $kualifikasi;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Kualifikasi\Kualifikasi  $kualifikasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kualifikasi $kualifikasi)
    {
        if (Kualifikasi::where([
                ['mkualNama', '=', $request->mkualNama],
                ['mkualStatus', '=', '1'],
                ['deleted_at', '=', 'NULL'],
                ['mkualId', '!=', $request->mkualId]
        ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Kualifikasi::where('mkualId', $request->mkualId)
              ->update([
                  'mkualNama' => $request->mkualNama,
                  'mkualStatus' => $request->mkualStatus,
            ]);
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil diubah !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Kualifikasi\Kualifikasi  $kualifikasi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Kualifikasi::destroy($id);
        if ($del){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil dihapus !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa dihapus !'], 200);
        }
    }
}
