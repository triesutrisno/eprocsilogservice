<?php

namespace App\Http\Controllers\Sendmail;

use App\Http\Controllers\Controller;
use App\Http\Models\Sendmail\Sendmail;
use Illuminate\Http\Request;

class SendmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Sendmail\Sendmail  $sendmail
     * @return \Illuminate\Http\Response
     */
    public function show(Sendmail $sendmail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Sendmail\Sendmail  $sendmail
     * @return \Illuminate\Http\Response
     */
    public function edit(Sendmail $sendmail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Sendmail\Sendmail  $sendmail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sendmail $sendmail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Sendmail\Sendmail  $sendmail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sendmail $sendmail)
    {
        //
    }
}
