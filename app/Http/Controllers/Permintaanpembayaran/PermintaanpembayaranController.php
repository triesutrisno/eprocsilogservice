<?php

namespace App\Http\Controllers\Permintaanpembayaran;

use App\Http\Controllers\Controller;
use App\Http\Models\Permintaanpembayaran\Permintaanpembayaran;
use App\Http\Models\Permintaanpembayaran\Permintaanpembayarandetail;
use Illuminate\Http\Request;
use App\Http\Models\Supplier\Supplier;
use App\Http\Models\Sendmail\Sendmail;

class PermintaanpembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->email!=""){
            $supp = Supplier::where(['supEmail'=>$request->email,'supStatus'=>'3'])->get();
            //return $supp;        
            $pp = Permintaanpembayaran::where(['supId'=>$supp[0]['supId']])->get();
        }else{
            $pp = Permintaanpembayaran::with(['supplier'])->get();
        }
        
        return $pp;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Permintaanpembayaran::where([
                'comp'=>$request->comp, 
                'noInvoice'=>$request->noInvoice,
                'status'=>'1'
            ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            $supp = Supplier::where(['supEmail'=>$request->userEmail,'supStatus'=>'3'])->get();
            $request->request->add(['supId'=>$supp[0]['supId']]);
            $request->request->add(['status'=>'1']);     
            $pp = Permintaanpembayaran::create($request->all()); 
            #dd($request->all());
            foreach($request->nilaiRequest as $key => $val){            
                $expl = explode('-',$key);
                $suppPPdetail = new Permintaanpembayarandetail;
                $suppPPdetail->ppId = $pp->ppId;            
                $suppPPdetail->noDokumen = $request->noDokumen;
                $suppPPdetail->typeDokumen = $request->typeDokumen;            
                $suppPPdetail->lineDokumen = $expl[1];
                $suppPPdetail->itemNumber = $expl[0];
                $suppPPdetail->itemName = $expl[6];
                $suppPPdetail->curency = $expl[2];
                $suppPPdetail->qty = $expl[3];
                $suppPPdetail->satuan = $expl[4];
                $suppPPdetail->totalNilai = $expl[5];
                $suppPPdetail->requestNilai = $request->nilaiRequest[$key];
                $suppPPdetail->status = "1";
                $suppPPdetail->userEmail = $request->userEmail;            
                $suppPPdetail->save();
            }
            if($pp){
                /** Send Email **/
                $isiEmail="<html>";
                $isiEmail.="<html>";
                $isiEmail.="<body>";
                $isiEmail.="<h3>Info Permintaan Pembayaran Supplier !</h3><br />";                
                $isiEmail.="Saat ini ada supplier yang mengajukan permintaan pembayaran di PT Semen Indonesia Group. <br />";
                $isiEmail.= "Silakan lakukan verifikasi atas data permintaan pembayaran dengan cara mengunjungi link dibawah ini <br />";
                $isiEmail.="http://eproc.silog.co.id <br />"; 
                $isiEmail.="Pakai user dan password anda untuk masuk kedalam sistem <br />"; 
                $isiEmail.="Terima Kasih <br /><br /><br /><br />"; 
                $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
                $isiEmail.= "</body>";
                $isiEmail.="</html>";

                $email = new Sendmail;
                $email->Sender = "sistem@silog.co.id";
                $email->Tanggal = date("Y-m-d H:i:s");
                $email->Recipients = "triesutrisno@silog.co.id";
                $email->CC = ""; 
                #$email->Recipients = "pengadaan@silog.co.id";
                #$email->CC = "triesutrisno@silog.co.id"; 
                $email->SubjectEmail = "Informasi Supplier Baru";
                $email->IsiEmail = addslashes($isiEmail);
                $email->Stat = "outbox";
                $email->email_password = "Veteran1974!@Gs";
                $email->ContentEmail = "0";
                $email->sistem = "eprocInfo";
                $email->save();
                return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
            }else{
                return response()->json(['kode'=>'90', 'pesan'=>'Data tidak berhasil disimpan, '], 200);
            }            
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data invoice sudah ada !'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayaran  $permintaanpembayaran
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pp = Permintaanpembayaran::with(['ppdetail','supplier'])
                  ->where(['ppId'=>$id])->get();
        return ['pp'=>$pp];
    }
    
    public function detail($id)
    {
        $pp = Permintaanpembayaran::with(['ppdetail'])
                  ->where(['ppId'=>$id])->get();
        return ['pp'=>$pp];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayaran  $permintaanpembayaran
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pp = Permintaanpembayaran::with(['ppdetail'])
                  ->where(['ppId'=>$id])->get();
        return ['pp'=>$pp];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayaran  $permintaanpembayaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(Permintaanpembayaran::where([
                ['comp', '=', $request->comp], 
                ['noInvoice', '=', $request->noInvoice],
                ['status', '=', '1'],
                ['ppId', '!=', $request->ppId]
            ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database  
            if($request->ppFile==""){
                Permintaanpembayaran::where('ppId', $request->ppId)
                  ->update([
                      'comp' => $request->comp,
                      'noInvoice' => $request->noInvoice,
                      'keterangan' => $request->keterangan,
                ]);
            }else{
                Permintaanpembayaran::where('ppId', $request->ppId)
                  ->update([
                      'comp' => $request->comp,
                      'noInvoice' => $request->noInvoice,
                      'keterangan' => $request->keterangan,
                      'ppFile' => $request->ppFile,
                ]);
            }
            foreach($request->nilaiRequest as $key => $val){                
                Permintaanpembayarandetail::where('ppDetailId', $key)
                ->update([
                    'requestNilai' => $request->nilaiRequest[$key],
              ]);
            }
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data invoice sudah ada !'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayaran  $permintaanpembayaran
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Permintaanpembayaran::destroy($id);
        if ($del){
            $del2 = Permintaanpembayarandetail::where(['ppId'=>$id])->delete();
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil dihapus !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa dihapus !'], 200);
        }
    }
    
    public function complite($id){
        $pp = Permintaanpembayaran::find(['ppId'=>$id]);
        //dd($pp[0]['tglVerifikasi']);
        if($pp[0]['status']=='1'){
            Permintaanpembayaran::where('ppId', $id)
              ->update([
                  'status' => '2',
                  'tglVerifikasi' => date("Y-m-d H:i:s"),
            ]);
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil diupdate !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa diupdate !'], 200);
        }
    }
    
    public function nocomplite($id){
        $pp = Permintaanpembayaran::with(['ppdetail','supplier'])
                  ->where(['ppId'=>$id])->get();
        //dd($pp[0]['tglVerifikasi']);
        return ['pp'=>$pp];
    }
    
    public function nocomplitepost(Request $request)
    {
        $pp = Permintaanpembayaran::find(['ppId'=>$request->ppId]);
        //dd($pp[0]['tglVerifikasi']);
        if($pp[0]['status']=='1'){
            Permintaanpembayaran::where('ppId', $request->ppId)
                ->update([
                    'status' => '3',
                    'tglVerifikasi' => date("Y-m-d H:i:s"),
                    'remark' => $request->keterangan,
              ]);

            /** Send Email **/

            $isiEmail="Pengajuan invoice anda tidak lolos verifikasi berikut adalah alasannya : <br />";
            $isiEmail.= $request->keterangan;         
            $isiEmail.= "<br /><br /><br />"; 
            $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
            $isiEmail.= "</body>";
            $isiEmail.="</html>";

            $email = new Sendmail;
            $email->Sender = "sistem@silog.co.id";
            $email->Tanggal = date("Y-m-d H:i:s");
            $email->Recipients = "triesutrisno@silog.co.id";
            $email->CC = "triesutrisno@silog.co.id";
            $email->SubjectEmail = "Verifikasi Permintaan Pembayaran";
            $email->IsiEmail = addslashes($isiEmail);
            $email->Stat = "outbox";
            $email->email_password = "Veteran1974!@Gs";
            $email->ContentEmail = "0";
            $email->sistem = "VerifikasiPP";
            $email->save();

            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa diupdate !'], 200);
        }
    }
    
    public function addnopppost(Request $request){
        $pp = Permintaanpembayaran::find(['ppId'=>$request->ppId]);
        //dd($pp[0]['tglVerifikasi']);
        if($pp[0]['status']=='2'){
            Permintaanpembayaran::where('ppId', $request->ppId)
              ->update([
                  'status' => '4',
                  'noPP' => $request->noPP,
                  'typeDokPP' => $request->typeDokPP,
            ]);
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil diupdate !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa diupdate !'], 200);
        }
    }
}
