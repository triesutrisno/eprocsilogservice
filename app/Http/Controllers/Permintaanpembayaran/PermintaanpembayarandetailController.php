<?php

namespace App\Http\Controllers\permintaanpembayaran;

use App\Http\Controllers\Controller;
use App\Http\Models\Permintaanpembayaran\Permintaanpembayarandetail;
use Illuminate\Http\Request;

class PermintaanpembayarandetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayarandetail  $permintaanpembayarandetail
     * @return \Illuminate\Http\Response
     */
    public function show(Permintaanpembayarandetail $permintaanpembayarandetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayarandetail  $permintaanpembayarandetail
     * @return \Illuminate\Http\Response
     */
    public function edit(Permintaanpembayarandetail $permintaanpembayarandetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayarandetail  $permintaanpembayarandetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permintaanpembayarandetail $permintaanpembayarandetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayarandetail  $permintaanpembayarandetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permintaanpembayarandetail $permintaanpembayarandetail)
    {
        //
    }
}
