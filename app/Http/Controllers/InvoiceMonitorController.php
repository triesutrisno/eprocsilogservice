<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\InvoiceMonitor;
use App\Http\Resources\InvoiceMonitorResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class InvoiceMonitorController extends Controller
{
    public function index(Request $request)
    {

        $per_page = $request->get('per_page', 10);
        $orderBy = $request->get('order_by', "id_invoice_monitor");
        $orderType = $request->get('order_type', "desc");
        $id_user = $request->get("id_user");

        $no_kwitansi = $request->get("no_kwitansi");
        $tgl_kwitansi = $request->get("tgl_kwitansi");
        $nama_perusahaan = $request->get("nama_perusahaan");

        $latestId = InvoiceMonitor::select(DB::raw('MAX(id_invoice_monitor) latest'));

        //FILTER when
        $user = User::find($id_user);

        $latestId->when($user->role  == 'supplier', function ($query) use ($user) {
            return $query->where('kode_sap_vendor', $user->supplier->kodeSAP);
        })
            ->when($nama_perusahaan, function ($query, $nama_perusahaan) {
                return $query->where('nama_perusahaan', 'LIKE', '%' . $nama_perusahaan . '%');
            })
            ->when($no_kwitansi, function ($query, $no_kwitansi) {
                return $query->where('no_kwitansi', 'LIKE', '%' . $no_kwitansi . '%');
            });

        if (request()->filled('tgl_kwitansi')) {
            $param = explode(" - ", $tgl_kwitansi);
            $start = Carbon::parse($param[0]);
            $end = Carbon::parse($param[1])->addDay();
            // dd($start);
            $latestId->whereBetween('tgl_kwitansi', [$start, $end]);
        }
        /////

        $latestId = $latestId->groupBy('no_kwitansi');

        $data = InvoiceMonitor::joinSub($latestId, 'latest_id', function ($join) {
            $join->on('id_invoice_monitor', '=', 'latest_id.latest');
        });


        $data = $data->orderBy($orderBy, $orderType)->paginate($per_page);
        return InvoiceMonitorResource::collection($data);
    }

    public function upload(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'uploaded_file' => 'required|mimes:xls,xlsx',
        ]);

        // dd($request->file('uploaded_files'));

        $message = "";
        $status = "";
        try {

            DB::transaction(function () use ($request, &$message, &$status) {


                $invoiceMonitorList = (new FastExcel)->import(
                    $request->file('uploaded_file'),
                    function ($line) {
                        return InvoiceMonitor::create([
                            'kode_sap_vendor' => $line['KODE SAP VENDOR'],
                            'nama_perusahaan' => $line['NAMA PERUSAHAAN'],
                            'nama_pelanggan' => $line['NAMA PELANGGAN'],
                            'no_kwitansi' => $line['NO KWITANSI'],
                            'nominal' => $line['NOMINAL'],
                            'tgl_kwitansi' => $line['TGL KWITANSI'],
                            'posisi_tagihan' => $line['POSISI TAGIHAN'],
                            'tgl_rencana_pembayaran' => empty($line['TGL RENCANA PEMBAYARAN']) ? NULL : $line['TGL RENCANA PEMBAYARAN'],

                        ]);
                    }
                );

                $status = "success";
                $message = "Berhasil Menyimpan";
            });
        } catch (\Exception $e) {
            $status = "error";
            $message = "Error!! " . $e->getMessage();
        }

        return response(['status' => $status, 'message' => $message], 200);
    }

    public function indexAll(Request $request)
    {
        $per_page = $request->get('per_page', 10);
        $orderBy = $request->get('order_by', "id_invoice_monitor");
        $orderType = $request->get('order_type', "desc");
        $id_user = $request->get("id_user");

        $no_kwitansi = $request->get("no_kwitansi");
        $tgl_kwitansi = $request->get("tgl_kwitansi");
        $nama_perusahaan = $request->get("nama_perusahaan");

        $data = InvoiceMonitor::query();

        $user = User::find($id_user);
        $data->when($user->role  == 'supplier', function ($query) use ($user) {
            return $query->where('kode_sap_vendor', $user->supplier->kodeSAP);
        })
            ->when($nama_perusahaan, function ($query, $nama_perusahaan) {
                return $query->where('nama_perusahaan', 'LIKE', '%' . $nama_perusahaan . '%');
            })
            ->when($no_kwitansi, function ($query, $no_kwitansi) {
                return $query->where('no_kwitansi', 'LIKE', '%' . $no_kwitansi . '%');
            });

        if (request()->filled('tgl_kwitansi')) {
            $param = explode(" - ", $tgl_kwitansi);
            $start = Carbon::parse($param[0]);
            $end = Carbon::parse($param[1])->addDay();
            // dd($start);
            $data->whereBetween('tgl_kwitansi', [$start, $end]);
        }

        $data = $data->orderBy($orderBy, $orderType)->paginate($per_page);
        return InvoiceMonitorResource::collection($data);
    }
}
