<?php

namespace App\Http\Controllers\Kategori;

use App\Http\Controllers\Controller;
use App\Http\Models\Kategori\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategoris = Kategori::all();
        return $kategoris;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Kategori::where(['mkatNama'=>$request->mkatNama, 'mkatStatus'=>'1', 'deleted_at'=>NULL])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Kategori::create($request->all());
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Kategori\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Kategori\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::where(['mkatId'=>$id])->get();
        return $kategori;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Kategori\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategori $kategori)
    {
        if (Kategori::where([
                ['mkatNama', '=', $request->mkatNama],
                ['mkatStatus', '=', '1'],
                ['mkatId', '!=', $request->mkatId]
        ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Kategori::where('mkatId', $request->mkatId)
              ->update([
                  'mkatNama' => $request->mkatNama,
                  'mkatStatus' => $request->mkatStatus,
            ]);
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil diubah !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Kategori\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Kategori::destroy($id);
        if ($del){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil dihapus !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa dihapus !'], 200);
        }
    }
}
