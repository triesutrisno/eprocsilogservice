<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Http\Models\Supplier\Supplier;
use Illuminate\Http\Request;
use App\User;
use App\Http\Models\Propinsi\Propinsi;
use App\Http\Models\Kabupaten\Kabupaten;
use App\Http\Models\Kecamatan\Kecamatan;
use App\Http\Models\Kategori\Kategori;
use App\Http\Models\Kualifikasi\Kualifikasi;
use App\Http\Models\Unit\Unit;
use App\Http\Models\Suppunit\Suppunit;
use Illuminate\Support\Facades\DB;
use App\Http\Models\Sendmail\Sendmail;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supp = Supplier::with(['kabupaten', 'kategori'])->get();
        return $supp;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $supp = Supplier::with(['propinsi', 'kabupaten', 'kecamatan', 'kategori', 'kualifikasi', 'pegawai'])
                ->where(['supId'=>$request->idsupp])
                ->get();
        return $supp;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $supp = Supplier::with(['propinsi', 'kabupaten', 'kecamatan', 'kategori', 'kualifikasi'])
                ->where(['supEmail'=>$request->email])
                ->get();
        
        $dtProp = Propinsi::select('mpropId','mpropNama')
                            ->where(['mpropStatus'=>'1'])->get();
        
        $dtKat = Kategori::select('mkatId','mkatNama')
                            ->where(['mkatStatus'=>'1'])->get();
        
        $dtKual = Kualifikasi::select('mkualId','mkualNama')
                            ->where(['mkualStatus'=>'1'])->get();
        
        $dtUnit = Unit::select('unitId', 'unit', 'keterangan')
                            ->where(['status'=>'1'])->get();
        
        return response()->json(['kode'=>'99', 'dtSupp'=>$supp, 'dtProp'=>$dtProp, 'dtKat'=>$dtKat, 'dtKual'=>$dtKual, 'dtUnit'=>$dtUnit], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $email)
    {
        $supp = Supplier::with(['propinsi', 'kabupaten', 'kecamatan', 'kategori', 'kualifikasi'])
                ->where(['supEmail'=>$request->email])
                ->get();
        
        //dd($supp);
        
        $lampiranSiup = $request->supSIUPFile != NULL ? $request->supSIUPFile : $supp[0]['supSIUPFile'];
        $lampiranNPWP = $request->supNPWPFile != NULL ? $request->supNPWPFile : $supp[0]['supNPWPFile'];
        $lampiranNPPKP = $request->supNPPKPFile != NULL ? $request->supNPPKPFile : $supp[0]['supNPPKPFile'];
        $lampiranTDP = $request->supTDPFile != NULL ? $request->supTDPFile : $supp[0]['supTDPFile'];
        $pemimpinKTPFile = $request->pemimpinKTPFile != NULL ? $request->pemimpinKTPFile : $supp[0]['pemimpinKTPFile'];
        $PemilikKTPFile = $request->PemilikKTPFile != NULL ? $request->PemilikKTPFile : $supp[0]['PemilikKTPFile'];    
        $supBank1File = $request->supBank1File != NULL ? $request->supBank1File : $supp[0]['supBank1File'];    
        $supBank2File = $request->supBank2File != NULL ? $request->supBank2File : $supp[0]['supBank2File'];    
        
        
        $upd = Supplier::where('supEmail', $email)
                ->update([
                    'supNama' => strtoupper($request->supNama),
                    'supWebsite' => $request->supWebsite,
                    'supTlp' => $request->supTlp,
                    'supAlamat' => $request->supAlamat,
                    'mpropId' => $request->mpropId,
                    'mkabId' => $request->mkabId,
                    'mkecId' => $request->mkecId,
                    'mkatId' => $request->mkatId,
                    'mkualId' => $request->mkualId,
                    'supSIUP' => $request->supSIUP,
                    'supSIUPFile' => $lampiranSiup,
                    'supNPWP' => $request->supNPWP,
                    'supNPWPFile' => $lampiranNPWP,
                    'supNPPKP' => $request->supNPPKP,
                    'supNPPKPFile' => $lampiranNPPKP,
                    'supTDP' => $request->supTDP,
                    'supTDPFile' => $lampiranTDP,
                    'supAkteNoteris' => $request->supAkteNoteris,
                    'supDomisili' => $request->supDomisili,
                    'supBank1' => $request->supBank1,
                    'supNorek1' => $request->supNorek1,
                    'supBank1File' => $supBank1File,
                    'supBank2' => $request->supBank2,
                    'supNorek2' => $request->supNorek2,
                    'supBank2File' => $supBank2File,
                    'pemimpinNama' => strtoupper($request->pemimpinNama),
                    'pemimpinJabatan' => $request->pemimpinJabatan,
                    'pemimpinEmail' => $request->pemimpinEmail,
                    'pemimpinTlp' => $request->pemimpinTlp,
                    'pemimpinHp' => $request->pemimpinHp,
                    'pemimpinAlamat' => $request->pemimpinAlamat,
                    'pemimpinKTP' => $request->pemimpinKTP,
                    'pemimpinKTPFile' => $pemimpinKTPFile,
                    'PemilikNama' => strtoupper($request->PemilikNama),
                    'PemilikJabatan' => $request->PemilikJabatan,
                    'PemilikEmail' => $request->PemilikEmail,
                    'PemilikTlp' => $request->PemilikTlp,
                    'PemilikHp' => $request->PemilikHp,
                    'PemilikAlamat' => $request->PemilikAlamat,
                    'PemilikKTP' => $request->PemilikKTP,
                    'PemilikKTPFile' => $PemilikKTPFile,
                    'supSetuju' => $request->supSetuju,
                ]);
        
        if($request->password!=NULL){
            $updUser = User::where('email', $email)
                ->update([
                    'password' => bcrypt($request->password),
                ]);
        }
        //dd($request->unit);
        if($request->unit<>NULL){            
            $del = DB::table('Suppunit')->where('supId', '=', $supp[0]['supId'])->delete();
            foreach($request->unit as $key => $val){
                    $suppUnit = new Suppunit();
                    $suppUnit->supId = $supp[0]['supId'];
                    $suppUnit->unitId = $key;
                    $suppUnit->save();
                }
            }

        return response()->json(['kode'=>'99', 'sup'=>$supp], 200);       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function verified(Request $request){
        $supp = Supplier::where(['supEmail'=>$request->email, 'supStatus'=>'2'])
                ->get();
        
        if(isset($supp[0]['supId'])){      
            $updSupp = Supplier::where('supId', $supp[0]['supId'])
                    ->update(['supStatus'=>'3']);
            if($updSupp){
                #$user = User::where('email', $request->email)
                #    ->update(['status'=>'3']);
                /** Insert ke tbale User **/
                $user = new User;
                $user->name = $supp[0]['supNama'];
                $user->email = $supp[0]['supEmail'];
                $user->role = "supplier";
                $user->status = "3";
                $user->password = bcrypt($supp[0]['has']);
                $user->save();

                $supp2 = Supplier::with(['propinsi', 'kabupaten', 'kecamatan', 'kategori', 'kualifikasi'])
                    ->where(['supEmail'=>$request->email])
                    ->get();
                
                /** Send Email **/
                $isiEmail="<html>";
                $isiEmail.="<html>";
                $isiEmail.="<body>";
                $isiEmail.="<h3>Terima Kasih !</h3><br />";                
                $isiEmail.="Selamat anda telah menjadi rekanan / vendor SILOG Group, berikut informasi user anda : <br />";
                $isiEmail.="Username = ".$supp[0]['supEmail']."<br />";
                $isiEmail.="Password = ".$supp[0]['has']."<br /><br />";
                $isiEmail.= "Silkana kunjungi eproc.silog.co.id dan gunakan user dan password anda untuk masuk dalama sistem";
                $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
                $isiEmail.= "</body>";
                $isiEmail.="</html>";
                
                $email = new Sendmail;
                $email->Sender = "sistem@silog.co.id";
                $email->Tanggal = date("Y-m-d H:i:s");
                $email->Recipients = $supp[0]['supEmail'];
                $email->CC = ""; 
                $email->SubjectEmail = "Notifikasi Akun Eproc Silog";
                $email->IsiEmail = addslashes($isiEmail);
                $email->Stat = "outbox";
                $email->email_password = "Veteran1974!@Gs";
                $email->ContentEmail = "0";
                $email->sistem = "eprocVerifikasi";
                $email->save();
                
                return response()->json(['kode'=>'99', 'data'=>$supp2, 'pesan'=>'Data berhasil diupdate !'], 200);
            }else{
                return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'Data tidak berhasil diupdate !'], 200);
            }
        }else{
            return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'Data tidak ditemukan !'], 200);
        }
    }
    
    public function notverified(Request $request){
        $supp = Supplier::where(['supEmail'=>$request->email, 'supStatus'=>'2'])
                ->get();
        //dd($supp);
        if(isset($supp[0]['supId'])){      
            $updSupp = Supplier::where('supId', $supp[0]['supId'])
                        ->update(['supStatus'=>'4', 'pesan'=>$request->pesan]);
            if($updSupp){
                #$user = User::where('email', $request->email)
                #    ->update(['status'=>'4']);
                
                $isiEmail="<html>";
                $isiEmail.="<html>";
                $isiEmail.="<body>";
                $isiEmail.="<h3>Mohon Maaf !</h3><br />";                
                $isiEmail.="Berdasarkan data yang ada kirim kepada kami,anda belum lolos sebagai vendor di Silog Group karena : <br />";
                $isiEmail.= $request->pesan;
                $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
                $isiEmail.= "</body>";
                $isiEmail.="</html>";
                
                $email = new Sendmail;
                $email->Sender = "sistem@silog.co.id";
                $email->Tanggal = date("Y-m-d H:i:s");
                $email->Recipients = $request->email;
                $email->CC = ""; 
                $email->SubjectEmail = "Informasi Validasi Eproc Silog Group";
                $email->IsiEmail = addslashes($isiEmail);
                $email->Stat = "outbox";
                $email->email_password = "Veteran1974!@Gs";
                $email->ContentEmail = "0";
                $email->sistem = "eprocVerifikasi";
                $email->save();

                $supp2 = Supplier::with(['propinsi', 'kabupaten', 'kecamatan', 'kategori', 'kualifikasi'])
                    ->where(['supEmail'=>$request->email])
                    ->get();
                return response()->json(['kode'=>'99', 'data'=>$supp2, 'pesan'=>'Data berhasil diupdate !'], 200);
            }else{
                return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'Data tidak berhasil diupdate !'], 200);
            }
        }else{
            return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'Data tidak ditemukan !'], 200);
        }
    }
}
