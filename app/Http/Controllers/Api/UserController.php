<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Log;


class UserController extends Controller
{    
    public $successStatus = 200;

    public function login(Request $request){
        $cek = User::select('email','status')->where(['email'=>$request->get('email')])->get()->toArray();
        $jml = count($cek);
        // Log::info("jml  ".$request->get('password'));

        if($jml>0){
            if ($cek['0']['status']=='3') { // Cek data apakah status user apakah 3 atau bukan
                if(Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])){
                    $user = Auth::user();
                    $success['id'] = $user->id;
                    $success['namaUser'] = $user->name;
                    $success['email'] = $user->email;
                    $success['role'] = $user->role;
                    $success['statusTMS'] = $user->statusTMS;
                    $success['token'] =  $user->createToken('nApp')->accessToken;
                    //$meta['token'] = $user->createToken('nApp')->accessToken;

                    return response()->json(['kode'=>'99', 'pesan'=>'', 'data' => $success], $this->successStatus);
                }
                else{
                    return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'User atau password tidak ditemukan'], 401);
                }

            }else if($cek['0']['status']=='1'){
                return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'Email belum diverifikasi, silakan verifikasi email anda !'], 401);
            }else if($cek['0']['status']=='2'){
                return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'User belum diverifikasi, silakan hubungi pengadaan silog group !'], 401);
            }else{
                return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'User tidak lolos verifikasi !'], 401);
            }
        }else{
            return response()->json(['kode'=>'90', 'data'=>'', 'pesan'=>'User tidak terdaftar !'], 401);
        }
    }
    
    public function logout(Request $request)
    {
        $user = User::where(['email'=> $request->email])->first();
        Auth::loginUsingId($user->id);
        $logout = Auth::user()->tokens()->delete();
        return response()->json($logout, 200);
    }

    public function register(Request $request)
    {
            $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|email|unique:users',
                    'password' => 'required',
                    'c_password' => 'required|same:password',
            ]);

            if ($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()], 401);            
            }

            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            if (User::where(['email'=>$request->email])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
                $user = User::create($input);
                $success['namaUser'] = $user->name;
                $success['email'] = $user->email;
                $success['api_token'] =  $user->createToken('nApp')->accessToken;
                return response()->json(['success'=>$success], $this->successStatus);
            }else{
                return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], $this->successStatus);
            }
    }

    public function details()
    {
            $user = Auth::user();
            return response()->json(['success' => $user], $this->successStatus);
    }
}
