<?php

namespace App\Http\Controllers\Prematchingtruk;

use App\Http\Controllers\Controller;
use App\Http\Models\Prematchingtruk\Prematchingtruk;
use Illuminate\Http\Request;
use App\Http\Models\Supplier\Supplier;
use App\Http\Models\Sendmail\Sendmail;
use App\Http\Models\Getdo\Getdo;
use App\Http\Models\Kesiapantruk\Kesiapantruk;
use App\Http\Models\Kesiapantruk\Tmsspp;
use App\Http\Models\Kesiapantruk\Nextnumber;
use App\Http\Models\Kesiapantruk\Matchingtruk;
use App\Http\Models\Kesiapantruk\Silogdojde;
use Illuminate\Support\Facades\DB;

class PrematchingtrukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        #$prematch = Prematchingtruk::where(['email'=>$request->email])->get();
        //return $supp;        
        //$pp = Permintaanpembayaran::where(['supId'=>$supp[0]['supId']])->get();
        
        #$nopin = $request->post("nopin") != NULL ? $request->post("nopin") : "";
        #$status = $request->post("status") != NULL ? $request->post("status") : "";
        
        $dtSupp = Supplier::where(['supEmail'=>$request->email])->get();
        
        #return $request->all();
        $data = DB::connection('PRODDTA')
            ->table('F5501011 A')
            ->selectRaw(
                'trim(A.SPDOCO) spdoco, A.SPDCTO, trim(B.SPDOC) spdoc, B.SPDCT, B.SPXLIN, trim(B.SPLITM) SPLITM, B.SPUOM, B.SPUORG, juliantodate(B.SPTRDJ) tglprematch,'
                .'juliantodate(B.SPDRQJ) tglrencanakirim, B.SPSHAN, trim(A.SPVEHI) SPVEHI, trim(A.SPDOCSO) SPDOCSO, A.SPAN8, trim(A.SPALPH) SPALPH, trim(IMDSC1) IMDSC1,'
                .'SLSSTS'
            )
            ->leftjoin('F5501012 B',['A.SPKCOO'=>'B.SPKCOO','A.SPDOCO'=>'B.SPDOCO','A.SPDCTO'=>'B.SPDCTO'])
            ->leftjoin('f4101 C',['B.SPLITM'=>'C.IMLITM'])
            ->leftjoin('F5501019 D',['A.SPKCOO'=>'D.SLKCOO','A.SPDOCO'=>'D.SLDOCO','A.SPDCTO'=>'D.SLDCTO'])
            ->whereRaw('B.SPDCT=? and A.SPCARS=?', ['DO',$dtSupp[0]['kodeJDE']])          
            #->when($nopin, function ($query, $nopin) {
            #        return $query->where(DB::raw("TRIM(SLVEHI)"), $nopin);
            #    })
            #->when($judul, function ($query, $judul) {
            #        return $query->where('sppJudul', 'LIKE', '%' . $judul . '%');
            #    })
            #->when($status, function ($query, $status) {
            #        return $query->where('SLSSTS', $status);
            #    })
            #->orderBy('a.sppNomer', 'asc')
            ->orderBy('SLSSTS', 'asc')
            ->get();

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $dtSupp = Supplier::where(['supEmail'=>$request->email])->get();
        $noDO = $request->post("noDO") != NULL ? $request->post("noDO") : "";
        if($noDO!=""){
            $expDO = explode("-",$noDO);
            $do     = $expDO[0];
            $type   = $expDO[2];
            $line   = $expDO[1];
        }else{
            $do     = "0";
            $type   = "";
            $line   = "0";
        }
        #return $dtSupp;
        
        $dtDO = Getdo::where([
                                'KODE_VENDOR'=>$dtSupp[0]['kodeJDE'],
                                'SDSHPN'=>'0'
                            ])
                        ->when($do, function ($query, $do) {
                                return $query->where("NO_DO", $do);
                            })
                        ->when($type, function ($query, $type) {
                                return $query->where("DO_TY", $type);
                            })
                        ->when($line, function ($query, $line) {
                                return $query->where("LINE_DO", $line);
                            })
                        ->get();
        
        $data = DB::connection('PRODDTA')
            ->table('VW_ARMADA P')
            ->selectRaw(
                'trim(NOPIN) NOPIN, trim(NOPOL) NOPOL, trim(KODE_DRV) KODE_DRV, trim(NAMA_DRV) NAMA_DRV, trim(NIK_DRV) NIK_DRV, trim(KODE_KUAT) KODE_KUAT, '
                    . 'trim(NAMA_SHIPTO) NAMA_SHIPTO, trim(SHIPTO_TERAKHIR) SHIPTO_TERAKHIR, trim(ALAMAT) ALAMAT, trim(KOTA) KOTA, VMMCU, trim(VMVOWN) VMVOWN, '
                    . 'trim(VMVTYP) VMVTYP, VMCVUM'
            )                
            ->whereRaw('FLAG_UTAMA = ? and VMVOWN=?', ['1',$dtSupp[0]['kodeJDE']])
            ->get();
        
        $dataSopir = DB::connection('PRODDTA')
            ->table('VW_ARMADA')
            ->selectRaw(
                'distinct trim(KODE_DRV) KODE_DRV,trim(NAMA_DRV) NAMA_DRV, NIK_DRV ABALKY'
            )
            #->whereRaw('KODE_DRV = ?', ['399999'])
            ->whereRaw('VMVOWN = ?', [$dtSupp[0]['kodeJDE']])
            ->get();
                
        return [
                    "dataDO"=>$dtDO,
                    "dataTruk"=>$data, 
                    "dataSopir"=>$dataSopir,
                ];
        
        #return $dtDO;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #dd($request->all());
        $nopol = $request->nopol;
        $driver = $request->driver;
        $doSilog = $request->doSilog;        
        
        $pesan = "";
        $no = 0;
        if($nopol!=""){
            $expNopol = explode("#", $nopol);
            $expDriver = explode("#", $driver);
            $expDoSilog  = explode("-", $doSilog);

            $kdVendor = $request->kdVendor!=NULL ? $request->kdVendor : "0";
            $nopin  = $expNopol[0]!=NULL ? $expNopol[0] : "0"; 
            $nopol  = $expNopol[1]!=NULL ? $expNopol[1] : " ";
            $kuat   = $expNopol[2]!=NULL ? $expNopol[2] : "0"; 
            $vmcvum = $expNopol[3]!=NULL ? $expNopol[3] : " "; 
            $kdUnit = $expNopol[4]!=NULL ? $expNopol[4] : "0"; 
            $kdCarrier = $expNopol[5]!=NULL ? $expNopol[5] : "0"; 
            $driver = $expDriver[0]!=NULL ? $expDriver[0] : "0"; 
            $nmDriver = $expDriver[1]!=NULL ? $expDriver[1] : " "; 
            $nikDriver = $expDriver[2]!=NULL ? $expDriver[2] : " "; 
            $asal      = $request->asal!=NULL ? $request->asal : "0";
            $motTransport = $expNopol[6]!=NULL ? $expNopol[6] : "";
            $tglRencanKirim = $request->tglRencanKirim!=NULL ? $request->tglRencanKirim : "";
            $kdShipto = $request->kdShipto!=NULL ? $request->kdShipto : "0";
            $kdWilayah = $request->kdWilayah!=NULL ? str_pad($request->kdWilayah, 12, " ", STR_PAD_LEFT) : " ";
            $item1 = $request->kdItem1!=NULL ? $request->kdItem1 : "0";
            $item2 = $request->kdItem2!=NULL ? $request->kdItem2 : " ";
            $item3 = $request->kdItem3!=NULL ? $request->kdItem3 : " ";
            $satuan = $request->satuan!=NULL ? $request->satuan : " ";
            $qty = $request->qty!=NULL ? $request->qty : "0";
            $branchplant = $request->branchplant!=NULL ? str_pad($request->branchplant, 12, " ", STR_PAD_LEFT) : " ";
            $userInput = "LOG_ADM";
            
            $rem = $request->rem!=NULL ? $request->rem : "4";
            $kemudi = $request->kemudi!=NULL ? $request->kemudi : "4"; 
            $ban = $request->ban!=NULL ? $request->ban : "4"; 
            $elektrik = $request->elektrik!=NULL ? $request->elektrik : "4"; 
            $kabin = $request->kabin!=NULL ? $request->kabin : "4"; 
            $kebocoran = $request->kebocoran!=NULL ? $request->kebocoran : "4"; 
            $kelengkapan = $request->kelengkapan!=NULL ? $request->kelengkapan : "4"; 
            $khusuDumpTruk = $request->khusuDumpTruk!=NULL ? $request->khusuDumpTruk : "4"; 
            $khususBulkTruk = $request->khususBulkTruk!=NULL ? $request->khususBulkTruk : "4"; 
            $khususBulkTruk2 = $request->khususBulkTruk2!=NULL ? $request->khususBulkTruk2 : "4"; 
            $setuju = $request->setuju!=NULL ? $request->setuju : "2";  

            $date       = date('Y-m-d');
            $time       = date('His');
            $thn        = (date('Y')-1900)*1000;
            $hari       = date('z', strtotime($date))+1;
            $dateJulian =  $thn+$hari;

            if (Kesiapantruk::where(DB::raw("TRIM(SLRLNO)"), $nopol)->where('SLSSTS','17')->doesntExist()) { // Cek data apakah sudah ada atau belum di database
                $dtNextnumber = Nextnumber::where([
                    'docType'=>"SP",
                    'status'=>1,
                ])->get(); 
                
                if($rem > "1"){
                     if($rem == "2"){
                        $pesan = "Rem nopol/nopin ".$nopol."/".$nopin." perlu perbaikan !";
                    }else{
                        $pesan = "Rem nopol/nopin ".$nopol."/".$nopin." tidak tersedia !";
                    }
                    return response()->json(['kode'=>'90', 'pesan'=>$pesan], 200);
                }elseif($kemudi > "1"){
                    if($kemudi == "2"){
                        $pesan = "Kemudi nopol/nopin ".$nopol."/".$nopin." perlu perbaikan !";
                    }else{
                        $pesan = "Kemudi nopol/nopin ".$nopol."/".$nopin." tidak tersedia !";
                    }
                    return response()->json(['kode'=>'90', 'pesan'=>$pesan], 200);
                }elseif($ban > "1"){
                    if($ban == "2"){
                        $pesan = "Ban nopol/nopin ".$nopol."/".$nopin." perlu perbaikan !";
                    }else{
                        $pesan = "Ban nopol/nopin ".$nopol."/".$nopin." tidak tersedia !";
                    }
                    return response()->json(['kode'=>'90', 'pesan'=>$pesan], 200);
                }elseif($elektrik > "1"){
                    if($elektrik == "2"){
                        $pesan = "Elektrik nopol/nopin ".$nopol."/".$nopin." perlu perbaikan !";
                    }else{
                        $pesan = "Elektrik nopol/nopin ".$nopol."/".$nopin."  tidak tersedia !";
                    }
                    return response()->json(['kode'=>'90', 'pesan'=>$pesan], 200);
                }elseif($kabin > "1"){
                    if($kabin == "2"){
                        $pesan = "Kabin nopol/nopin ".$nopol."/".$nopin." perlu perbaikan !";
                    }else{
                        $pesan = "Kabin nopol/nopin ".$nopol."/".$nopin." tidak tersedia !";
                    }
                    return response()->json(['kode'=>'90', 'pesan'=>$pesan], 200);
                }elseif($kebocoran > "1"){
                    if($kebocoran == "2"){
                        $pesan = "Kebocoran perlu perbaikan !";
                    }else{
                        $pesan = "Kebocoran tidak tersedia !";
                    }
                    return response()->json(['kode'=>'90', 'pesan'=>$pesan], 200);
                }elseif($kelengkapan > "1"){
                    if($kelengkapan == "2"){
                        $pesan = "Kelengkapan nopol/nopin ".$nopol."/".$nopin." perlu perbaikan !";
                    }else{
                        $pesan = "Kelengkapan nopol/nopin ".$nopol."/".$nopin." tidak tersedia !";
                    }
                    return response()->json(['kode'=>'90', 'pesan'=>$pesan], 200);
                }
                
                $jmlNext = count($dtNextnumber);
                if($jmlNext>0){
                    $nomer = sprintf("%08d",$dtNextnumber[0]['nextNumber']);

                    $update = Nextnumber::where('numberId', $dtNextnumber[0]['numberId'])
                        ->update([
                            'nextnumber' => $dtNextnumber[0]['nextNumber']+1,
                      ]);
                }

                try {
                    $trukSiap = new Kesiapantruk();
                    $trukSiap->SLDOCO = $nomer;
                    $trukSiap->SLDCTO = "SP";
                    $trukSiap->SLKCOO = "10100";
                    $trukSiap->SLVEHI = $nopin;
                    $trukSiap->SLRLNO = $nopol;
                    $trukSiap->SLORGN = $asal;
                    $trukSiap->SLVEND = $asal;
                    $trukSiap->SLSHAN = $kdShipto;
                    $trukSiap->SLANID = "0";
                    $trukSiap->SLSSTS = "17";
                    $trukSiap->SLTRDJ = $dateJulian;
                    $trukSiap->SLTRTM = $time; 
                    $trukSiap->SLDPDT = "0";
                    $trukSiap->SLDETM = "0";
                    $trukSiap->SLLDDT = "0";
                    $trukSiap->SLLDTM = "0";
                    $trukSiap->SLDLDT = "0";
                    $trukSiap->SLDLTM = "0"; 
                    $trukSiap->SLADDJ = "0";
                    $trukSiap->SLADTM = "0";
                    $trukSiap->SLRMK = " "; 
                    $trukSiap->SLCDAA = $kuat; 
                    $trukSiap->SLCDBB = $vmcvum; 
                    $trukSiap->SLCDCC = " ";
                    $trukSiap->SLUSAP = $userInput; 
                    $trukSiap->SLAPPJ = "0";
                    $trukSiap->SLATIM = "0";
                    $trukSiap->SLAA1 = "0";
                    $trukSiap->SLAA2 = "0";
                    $trukSiap->SLAA3 = "0";
                    $trukSiap->SLTORG = $userInput; 
                    $trukSiap->SLCRDJ = $dateJulian;
                    $trukSiap->SLCRTM = $time; 
                    $trukSiap->SLUSER = $userInput; 
                    $trukSiap->SLUPMJ = $dateJulian;
                    $trukSiap->SLUPMT = $time;  
                    $trukSiap->SLJOBN = "EPROC"; 
                    $trukSiap->SLPID = "APREM"; 
                    $trukSiap->SLURCD = " "; 
                    $trukSiap->SLURAT = "0"; 
                    $trukSiap->SLURRF = " "; 
                    $trukSiap->SLURDT = "0"; 
                    $trukSiap->SLURAB = "0"; 
                    $trukSiap->SLAN8 = $driver; 
                    $trukSiap->SLALPH = $nmDriver; 
                    $trukSiap->SLALKY = $nikDriver; 
                    #$tmsSpp->SLCARS = $kdCarrier;
                    $trukSiap->save();

                    $tmsSpp = new Tmsspp();
                    $tmsSpp->SPKCOO = "10100";
                    $tmsSpp->SPDCTO = "SP";
                    $tmsSpp->SPDOCO = $nomer;
                    $tmsSpp->SPMCU = $branchplant; 
                    $tmsSpp->SPTRDJ = $dateJulian;
                    $tmsSpp->SPDRQJ = $dateJulian;
                    $tmsSpp->SPAN8 = $driver; 
                    $tmsSpp->SPALPH = $nmDriver; 
                    $tmsSpp->SPEMCU = $kdWilayah; 
                    $tmsSpp->SPCARS = $kdVendor;
                    $tmsSpp->SPVEHI = $nopin; 
                    $tmsSpp->SPMOT = $motTransport;
                    $tmsSpp->SPSHAN = $kdShipto;
                    $tmsSpp->SPLOCN = " ";
                    $tmsSpp->SPUORG = "0";
                    $tmsSpp->SPUOM = " ";
                    $tmsSpp->SPAA = "0";
                    $tmsSpp->SPAA1 = "0";
                    $tmsSpp->SPKCO = " ";
                    $tmsSpp->SPDCT = " ";
                    $tmsSpp->SPDOC = "0";
                    $tmsSpp->SPDOCSO = $nopol;
                    $tmsSpp->SPDOCSPJ = " ";
                    $tmsSpp->SPEV01 = " ";
                    $tmsSpp->SPEV02 = " ";
                    $tmsSpp->SPEV03 = " ";
                    $tmsSpp->SPEV04 = " ";
                    $tmsSpp->SPEV05 = " ";
                    $tmsSpp->SPEV06 = " ";
                    $tmsSpp->SPPID = "APREM";
                    $tmsSpp->SPJOBN = "EPROC";
                    $tmsSpp->SPUSER = $userInput; 
                    $tmsSpp->SPUPMJ = $dateJulian;
                    $tmsSpp->SPUPMT = $time;
                    $tmsSpp->SPUPDT = " ";
                    $tmsSpp->save();

                    $prematch = new Matchingtruk();
                    $prematch->SPKCOO = "10100";
                    $prematch->SPDCTO = "SP";
                    $prematch->SPDOCO = $nomer;
                    $prematch->SPLNID = "1000";
                    $prematch->SPTRDJ = $dateJulian;
                    $prematch->SPDRQJ = $tglRencanKirim;
                    $prematch->SPSHAN = $kdShipto; 
                    #$prematch->SPMCU = $kdUnit; 
                    $prematch->SPEMCU = $kdWilayah; 
                    $prematch->SPITM = $item1; 
                    $prematch->SPLITM = $item2;
                    $prematch->SPAITM = $item3; 
                    $prematch->SPLOCN = " ";
                    $prematch->SPUOM = $satuan;
                    $prematch->SPUORG = $qty;
                    $prematch->SPAA = "0";
                    $prematch->SPAA1 = "0";
                    $prematch->SPKCO = "10100";
                    $prematch->SPDCT = $expDoSilog[2];
                    $prematch->SPDOC = $expDoSilog[0];
                    $prematch->SPXLIN = $expDoSilog[1];
                    $prematch->SPEV01 = " ";
                    $prematch->SPEV02 = " ";
                    $prematch->SPEV03 = " ";
                    $prematch->SPEV04 = " ";
                    $prematch->SPEV05 = " ";
                    $prematch->SPEV06 = " ";
                    $prematch->SPPID = "APREM";
                    $prematch->SPJOBN = "EPROC";
                    $prematch->SPUSER = $userInput; 
                    $prematch->SPUPMJ = $dateJulian;
                    $prematch->SPUPMT = $time;
                    $prematch->save();

                    $updDO = Silogdojde::where(['SDDOCO' => $expDoSilog[0], 'SDDCTO'=>$expDoSilog[2], 'SDLNID'=>$expDoSilog[1], 'SDKCOO'=>'10100'])
                    ->update([
                        'SDCARS' => $kdVendor,
                        'SDMOT' => $motTransport,
                        'SDSHPN' => $nomer,
                    ]);

                    $no++;
                    $pesan .= "Nopol/Nopin ".$nopol."/".$nopin." berhasil prematching ! \n";
                    #$pesan3.="=================================================================== \n";            
                    $tokenBot = '5176084866:AAFWUdAjz-od7X204IO3SZiHR84F-pS6u6E';
                    $pesan3 = "<b>Notifikasi Prematching</b>\n";
                    $pesan3.= $pesan." \n";
                    $pesan3.= "Terima Kasih.";

                    //--- Start Kirim Ke Bot Telegram Ke group TMS SILOG OPERASIONAL ---//
                    $idGroupTelegram = "-762323495";
                    $url = "https://api.telegram.org/bot".$tokenBot."/sendMessage";
                    $isiPesan3 = $pesan3;
                    $parameter=[
                            'chat_id'=> $idGroupTelegram,
                            'text'=>$isiPesan3,
                            'parse_mode'=>'HTML',
                            ];
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, ($parameter));
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $data = curl_exec($ch);
                    curl_close($ch);
                    #return response()->json(['kode'=>'99', 'pesan'=> $data], 200);;
                    return response()->json(['kode'=>'99', 'pesan'=> $pesan], 200);
                }
                catch(Exception $e) {
                    $pesan .= $e->getMessage();
                }
            }else{
                return response()->json(['kode'=>'90', 'pesan'=>"Nopol/Nopin ".$nopol."/".$nopin." sudah ada !"], 200);
            } 
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Inputan tidak boleh kosong !'], 200);
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayaran  $permintaanpembayaran
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayaran  $permintaanpembayaran
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayaran  $permintaanpembayaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Permintaanpembayaran\Permintaanpembayaran  $permintaanpembayaran
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$doc,$dct,$lnid)
    {        
        $del = Kesiapantruk::where(['SLDOCO'=>$id, 'SLDCTO'=>'SP'])->delete();
        $del2 = Tmsspp::where(['SPDOCO'=>$id, 'SPDCTO'=>'SP'])->delete();
        $del3 = Matchingtruk::where(['SPDOCO'=>$id, 'SPDCTO'=>'SP'])->delete();
        
        $updDO = Silogdojde::where(['SDDOCO' => $doc, 'SDDCTO'=>$dct, 'SDLNID'=>$lnid, 'SDKCOO'=>'10100'])
                ->update([
                    'SDSHPN' => "0",
                ]);
        
        if ($del){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil dihapus !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa dihapus !'], 200);
        }
    }
}
