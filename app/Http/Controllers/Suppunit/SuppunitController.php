<?php

namespace App\Http\Controllers\Suppunit;

use App\Http\Controllers\Controller;
use App\Http\Models\Suppunit\Suppunit;
use Illuminate\Http\Request;

class SuppunitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Suppunit\Suppunit  $suppunit
     * @return \Illuminate\Http\Response
     */
    public function show(Suppunit $suppunit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Suppunit\Suppunit  $suppunit
     * @return \Illuminate\Http\Response
     */
    public function edit(Suppunit $suppunit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Suppunit\Suppunit  $suppunit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suppunit $suppunit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Suppunit\Suppunit  $suppunit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suppunit $suppunit)
    {
        //
    }
}
