<?php

namespace App\Http\Controllers\Tender;

use App\Http\Controllers\Controller;
use App\Http\Models\Tender\Tender;
use Illuminate\Http\Request;
use App\Http\Models\Kategori\Kategori;
use App\Http\Models\Supplier\Supplier;
use App\Http\Models\Currency\Currency;
use App\Http\Models\Sendmail\Sendmail;
use App\Http\Models\Tender\Tenderdetail;
use App\Http\Models\Nextnumber\Nextnumber;
use App\Http\Models\Unit\Unit;

class TenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tender = Tender::with(['kategori', 'supplier', 'currency'])->get();
        return $tender;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->tenderJenis=3){
            $jns = "PJ";
        }else{
            $jns = "PB";
        }
        if (Tender::where([
                'tenderNama'=>$request->tenderNama, 
                'comp'=>$request->comp, 
                'tenderTahun'=>$request->tenderTahun, 
                'unitId'=>$request->unitId,
            ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database
            //mencariKodeUnit
            $unit = Unit::where(['unitId'=>$request->unitId])->get();
            
            // Nextnumber Start
            $dtNextnumber = Nextnumber::where([
                'comp'=>$request->comp, 
                'tahun'=>$request->tenderTahun,
                'kode'=>$unit[0]['kode'],
                'jenis'=> $jns,
                'status'=>1,
            ])->get();
            $jmlNext = count($dtNextnumber);
            if($jmlNext>0){
                $nomer = sprintf("%05d",$dtNextnumber[0]['urutan']);
                $nextnumber = $jns."/".$request->comp."/".$unit[0]['kode']."/".$request->tenderTahun."/".date("m")."/".$nomer;

                if (Tender::where([
                    'tenderNomor'=>$nextnumber,
                ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database
                    $update = Nextnumber::where('id', $dtNextnumber[0]['id'])
                                ->update([
                                    'urutan' => $dtNextnumber[0]['urutan']+1,
                              ]);
                    $request->request->add(['tenderNomor'=>$nextnumber]);
                    $simpan = Tender::create($request->all());
                    //return response()->json(['request'=>$request->all()]);
                    return response()->json(['kode'=>'99', 'tenderId'=>$simpan->tenderId, 'pesan'=>'Data berhasil disimpan !'], 200);
                }else{
                    return response()->json(['kode'=>'90', 'pesan'=>'Data nextnumber sudah ada !'], 200);
                }
            }else{
                return response()->json(['kode'=>'90', 'pesan'=>'Data nextnumber comp '.$request->comp.' tahun '.$request->tenderTahun.' kode '.$unit[0]['kode'].' belum disetting !'], 200);
            }
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Tender\Tender  $tender
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tender = Tender::with(['kategori', 'supplier'])
                  ->where(['tenderId'=>$id])->get();
        
        $tenderDetail = Tenderdetail::with(['tender','supplier', 'email'])
                  ->where(['tenderId'=>$id])->get();        
        $dtPengikut=$dtNego= array();
        foreach($tenderDetail as $val){
            //return ['dtPengikut'=>$val];
            if($val['penawaranStatus']>='2'){
                $pengikut['tenderId'] = $val['tender']['0']['tenderId'];
                $pengikut['comp'] = $val['tender']['0']['comp'];
                $pengikut['tenderTahun'] = $val['tender']['0']['tenderTahun'];
                $pengikut['tenderNama'] = $val['tender']['0']['tenderNama'];
                $pengikut['currency'] = $val['tender']['0']['tenderCurency'];
                $pengikut['harga'] = $val['tender']['0']['tenderBudget'];
                $pengikut['suppId'] = $val['supId'];
                $pengikut['supNama'] = $val['supplier']['0']['supNama'];
                $pengikut['penawaranId'] = $val['tenderPenawaranId'];
                $pengikut['spesifikasi'] = $val['penawaranSpesifikasi'];
                $pengikut['currencyPenawaran'] = $val['penawaranCurency'];
                $pengikut['hargaPenawaran'] = $val['penawaranHarga'];
                $pengikut['hargaNegosiasi'] = $val['penawaranNego'];                
                $pengikut['tglNegoAwal'] = $val['tender']['0']['tglNegoAwal'];
                $pengikut['tglNegoAkhir'] = $val['tender']['0']['tglNegoAkhir'];
                $pengikut['tglNegosiasi'] = $val['tglNego'];
                $pengikut['expired'] = $val['penawaranExpired'];
                $pengikut['file'] = $val['penawaranFile'];
                $pengikut['gambar'] = $val['penawaranGambar'];
                $pengikut['penawaranStatus'] = $val['penawaranStatus'];

                $dtPengikut[]= $pengikut;
            }
            /*else if($val['penawaranStatus']=='3'){
                $nego['tenderId'] = $val['tender']['0']['tenderId'];
                $nego['comp'] = $val['tender']['0']['comp'];
                $nego['tenderTahun'] = $val['tender']['0']['tenderTahun'];
                $nego['tenderNama'] = $val['tender']['0']['tenderNama'];
                $nego['currency'] = $val['tender']['0']['tenderCurency'];
                $nego['harga'] = $val['tender']['0']['tenderBudget'];
                $nego['suppId'] = $val['supId'];
                $nego['supNama'] = $val['supplier']['0']['supNama'];
                $nego['penawaranId'] = $val['tenderPenawaranId'];
                $nego['spesifikasi'] = $val['penawaranSpesifikasi'];
                $nego['currencyPenawaran'] = $val['penawaranCurency'];
                $nego['hargaPenawaran'] = $val['penawaranHarga'];
                $nego['expired'] = $val['penawaranExpired'];
                $nego['file'] = $val['penawaranFile'];
                $nego['gambar'] = $val['penawaranGambar'];
                $nego['penawaranStatus'] = $val['penawaranStatus'];

                $dtNego[]= $nego;
            }*/
        }

        $supp = Supplier::where(['supStatus'=>'3'])->get();
        
        return ['tender'=>$tender, 'supp'=>$supp, 'tenderDetail'=>$tenderDetail, 'dtPengikut'=>$dtPengikut, 'dtNego'=>$dtNego];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Tender\Tender  $tender
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kat = Kategori::all();
        $curr = Currency::all();   
        
        $tender = Tender::with(['kategori', 'supplier'])
                  ->where(['tenderId'=>$id])->get();
        return ['tender'=>$tender, 'kat'=>$kat, 'curr'=>$curr];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Tender\Tender  $tender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tender $tender)
    {
        $tender = Tender::where(['tenderId'=>$request->tenderId])
                ->get();

        $tenderFile = $request->tenderFile != NULL ? $request->tenderFile : $tender[0]['tenderFile'];
        $tenderGambar = $request->tenderGambar != NULL ? $request->tenderGambar : $tender[0]['tenderGambar'];

        if (Tender::where([
                ['tenderNama', '=', $request->tenderNama],
                ['comp', '=', $request->comp],
                ['tenderTahun', '=', $request->tenderTahun],
                ['unitId', '=', $request->unitId],
                ['deleted_at', '=', 'NULL'],
                ['tenderId', '!=', $request->tenderId]
        ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Tender::where('tenderId', $request->tenderId)
              ->update([
                  'comp' => $request->comp,
                  'tenderTahun' => $request->tenderTahun,
                  'tenderNama' => $request->tenderNama,
                  'tenderJenis' => $request->tenderJenis,
                  'tenderSpesifikasi' => $request->tenderSpesifikasi,
                  'tenderCurency' => $request->tenderCurency,
                  'tenderBudget' => $request->tenderBudget,
                  'statusBudget' => $request->statusBudget,
                  'tenderType' => $request->tenderType,
                  'mkatId' => $request->mkatId,
                  'tenderReff1' => $request->tenderReff1,
                  'tenderReff2' => $request->tenderReff2,
                  'tenderFile' => $tenderFile,
                  'tenderGambar' => $tenderGambar,
                  'tglTutup' => $request->tglTutup,
                  'tglAnwijzing' => $request->tglAnwijzing,
                  'lokasiAnwijzing' => $request->lokasiAnwijzing,
            ]);
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil diubah !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Tender\Tender  $tender
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Tender::destroy($id);
        if ($del){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil dihapus !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa dihapus !'], 200);
        }
    }
    
    public function kirimemail(Request $request)
    {
        $kategori = $request->kategori!="" ? $request->kategori : "";
        $vendor = $request->vendor!="" ? $request->vendor : "";
        
        #return response()->json(['vendor'=>$vendor], 200);
        
        $tender = Tender::with(['kategori'])
                  ->where(['tenderId'=>$request->tenderId])->get();
        
        if($tender[0]['tenderType']=='1'){
            $typeTender = "Tender";
        }elseif($tender[0]['tenderType']=='2'){
            $typeTender = "Non Tender";            
        }else{
            $typeTender = "Repead Orders";            
        }
        
        
        /** Send Email **/
        $isiEmail="<html>";
        $isiEmail.="<html>";
        $isiEmail.="<body>";
        $isiEmail.="Semen Indonesia Logisitk Group mengundang Bapak/Ibu untuk mengikuti proses pengadaan <br />";
        $isiEmail.="<table style='border:0;bordercolor=#ffffff' width='100%'>";
        $isiEmail.="<tr>";
        $isiEmail.="<td width='40'>Nomer</td>";
        $isiEmail.="<td width='10'>:</td>";
        $isiEmail.="<td>".$tender[0]['tenderNomor']."</td>";
        $isiEmail.="</tr>";
        $isiEmail.="<tr>";
        $isiEmail.="<td>Deskripsi</td>";
        $isiEmail.="<td>:</td>";
        $isiEmail.="<td>".$tender[0]['tenderNama']."</td>";
        $isiEmail.="</tr>";
         $isiEmail.="<tr>";
        $isiEmail.="<td>Type Kegiatan</td>";
        $isiEmail.="<td>:</td>";
        $isiEmail.="<td>".$typeTender."</td>";
        $isiEmail.="</tr>";   
        $isiEmail.="<tr>";
        $isiEmail.="<td>Tanggal Anwijzing</td>";
        $isiEmail.="<td>:</td>";
        $isiEmail.="<td>".$tender[0]['tglAnwijzing']."</td>";
        $isiEmail.="<tr>";
        $isiEmail.="<td>Lokasi Anwijzing</td>";
        $isiEmail.="<td>:</td>";
        $isiEmail.="<td>".$tender[0]['lokasiAnwijzing']."</td>";
        $isiEmail.="</tr>";
        $isiEmail.="<tr>";
        $isiEmail.="<td>Batas Penawaran</td>";
        $isiEmail.="<td>:</td>";
        $isiEmail.="<td>".$tender[0]['tglTutup']."</td>";
        $isiEmail.="</tr>";
        $isiEmail.="</table><br />";
        $isiEmail.="Silakan akses eproc.silog.co.id dan gunakan user dan password anda untuk login ke aplikasi. <br />";
        $isiEmail.="Untuk informasi lebih lanjut, silakan kirim email ke pengadaan@silog.co.id<br /><br />";
        $isiEmail.="Terima Kasih <br /><br /><br />";
        $isiEmail.="Bagian Pengadaan <br />";
        $isiEmail.="Semen Indonesia Logistik Group <br />";
        $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
        $isiEmail.= "</body>";
        $isiEmail.="</html>";
        
        if($kategori!=""){
            $supp = Supplier::where(['supStatus'=>'3','mkatId'=>$kategori])->get();
            $jmlSupp = count($supp);
            if($jmlSupp>0){
                foreach($supp as $isi){
                    $cek = Tenderdetail::where(['tenderId'=>$request->tenderId,'supId'=>$isi['supId']])->get();
                    $jmlCek = count($cek);
                    if($jmlCek==""){
                        $tenderDetail = new Tenderdetail;
                        $tenderDetail->tenderId = $request->tenderId;
                        $tenderDetail->supId = $isi['supId'];
                        $tenderDetail->penawaranStatus = "1";
                        $tenderDetail->save();

                        /** Send Email **/
                        $email = new Sendmail;
                        $email->Sender = "sistem@silog.co.id";
                        $email->Tanggal = date("Y-m-d H:i:s");
                        $email->Recipients = $isi['supEmail'];
                        $email->CC = ""; 
                        $email->SubjectEmail = "Undangan Pengadaan dari Semen Indonesia Logistik Group";
                        $email->IsiEmail = addslashes($isiEmail);
                        $email->Stat = "outbox";
                        $email->email_password = "Veteran1974!@Gs";
                        $email->ContentEmail = "0";
                        $email->sistem = "eprocUndangan";
                        $email->refId = $tenderDetail->tenderPenawaranId;
                        $email->save();
                    }
                }
            }
        }        
        if($vendor!=""){
            foreach($vendor as $val){
                $supp2 = Supplier::where(['supStatus'=>'3', 'supId'=>$val])->get();
                $jmlSupp2 = count($supp2);
                if($jmlSupp2>0){
                    $cek2 = Tenderdetail::where(['tenderId'=>$request->tenderId,'supId'=>$val])->get();
                    $jmlCek2 = count($cek2);
                    if($jmlCek2=="" || $jmlCek2=="0"){
                        $tenderDetail = new Tenderdetail;
                        $tenderDetail->tenderId = $request->tenderId;
                        $tenderDetail->supId = $supp2[0]['supId'];
                        $tenderDetail->penawaranStatus = "1";
                        $tenderDetail->save();

                        /** Send Email **/
                        $email2 = new Sendmail;
                        $email2->Sender = "sistem@silog.co.id";
                        $email2->Tanggal = date("Y-m-d H:i:s");
                        $email2->Recipients = $supp2[0]['supEmail'];
                        $email2->CC = ""; 
                        $email2->SubjectEmail = "Undangan Pengadaan dari Semen Indonesia Logistik Group";
                        $email2->IsiEmail = addslashes($isiEmail);
                        $email2->Stat = "outbox";
                        $email2->email_password = "Veteran1974!@Gs";
                        $email2->ContentEmail = "0";
                        $email2->sistem = "eprocUndangan";
                        $email2->refId = $tenderDetail->tenderPenawaranId;
                        $email2->save();
                    }
                }
            }
        }
        
        $updTender = Tender::where('tenderId', $request->tenderId)
                   ->update(['tenderStatus'=>'2']);
         
        return response()->json(['kode'=>'99', 'pesan'=>'Email berhasil dikirim !'], 200);
    }
    public function waktunego(Request $request)
    {
        $supplier = $request->supplier!="" ? $request->supplier : "";
        
        $update = Tender::where('tenderId', $request->tenderId)
              ->update([
                  'tglNegoAwal' => $request->tglNegoAwal,
                  'tglNegoAkhir' => $request->tglNegoAkhir,
                  'tenderStatus' => '3',
            ]);  
        
        if($tenderDetail[0]['tender'][0]['tenderType']=='1'){
            $typeTender = "Tender";
        }elseif($tenderDetail[0]['tender'][0]['tenderType']=='2'){
            $typeTender = "Non Tender";            
        }else{
            $typeTender = "Repead Orders";            
        }
        
        if($update){            
            if($supplier!=""){
                foreach($supplier as $val){
                    $tenderDetail = Tenderdetail::with(['tender','supplier'])
                            ->where(['tenderId'=>$request->tenderId, 'penawaranStatus'=>'2', 'supId'=>$val])->get();  
                    //return $tenderDetail;
                    /** Send Email **/
                    $isiEmail="<html>";
                    $isiEmail.="<html>";
                    $isiEmail.="<body>";
                    $isiEmail.="Bersama dengan email ini kami mengundang Bapak/Ibu untuk mengikuti proses negosiasi harga atas kegitan pengadaan kami : <br />";
                    $isiEmail.="<table style='border:0;bordercolor=#ffffff' width='100%'>";
                    $isiEmail.="<tr>";
                    $isiEmail.="<td width='40'>Nomer</td>";
                    $isiEmail.="<td width='10'>:</td>";
                    $isiEmail.="<td>".$tenderDetail[0]['tender'][0]['tenderNomor']."</td>";
                    $isiEmail.="</tr>";
                    $isiEmail.="<tr>";
                    $isiEmail.="<td>Deskripsi</td>";
                    $isiEmail.="<td>:</td>";
                    $isiEmail.="<td>".$tenderDetail[0]['tender'][0]['tenderNama']."</td>";
                    $isiEmail.="</tr>";                    
                    $isiEmail.="<tr>";
                    $isiEmail.="<td>Type Kegiatan</td>";
                    $isiEmail.="<td>:</td>";
                    $isiEmail.="<td>".$typeTender."</td>";
                    $isiEmail.="</tr>";   
                    $isiEmail.="<tr>";
                    $isiEmail.="<td>Tanggal Nego Awal</td>";
                    $isiEmail.="<td>:</td>";
                    $isiEmail.="<td>".$tenderDetail[0]['tender'][0]['tglNegoAwal']."</td>";
                    $isiEmail.="<tr>";
                    $isiEmail.="<td>Tanggal Nego Akhir</td>";
                    $isiEmail.="<td>:</td>";
                    $isiEmail.="<td>".$tenderDetail[0]['tender'][0]['tglNegoAkhir']."</td>";
                    $isiEmail.="</tr>";
                    $isiEmail.="</table><br />";
                    $isiEmail.="Silakan akses eproc.silog.co.id dan gunakan user dan password anda untuk login ke aplikasi. <br />";
                    $isiEmail.="Untuk informasi lebih lanjut, silakan kirim email ke pengadaan@silog.co.id<br /><br />";
                    $isiEmail.="Terima Kasih <br /><br /><br />";
                    $isiEmail.="Bagian Pengadaan <br />";
                    $isiEmail.="Semen Indonesia Logistik Group <br />";
                    $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
                    $isiEmail.= "</body>";
                    $isiEmail.="</html>"; 
                    
                    $email = new Sendmail;
                    $email->Sender = "sistem@silog.co.id";
                    $email->Tanggal = date("Y-m-d H:i:s");
                    $email->Recipients = $tenderDetail[0]['supplier'][0]['supEmail'];
                    $email->CC = ""; 
                    $email->SubjectEmail = "Undangan Nego Harga dari Semen Indonesia Logistik Group";
                    $email->IsiEmail = addslashes($isiEmail);
                    $email->Stat = "outbox";
                    $email->email_password = "Veteran1974!@Gs";
                    $email->ContentEmail = "0";
                    $email->sistem = "eprocUndanganNego";
                    $email->refId = $tenderDetail[0]['tenderPenawaranId'];
                    $email->save();
                        
                    $updTenderDetail = Tenderdetail::where(['tenderId'=>$request->tenderId, 'supId'=>$val])
                                        ->update([
                                            'penawaranStatus' => '3',
                                      ]);
                }
            }  
            $updTenderDetail2 = Tenderdetail::where(['tenderId'=>$request->tenderId,'penawaranStatus'=>'2'])
                  ->update([
                      'penawaranStatus' => '5',
                ]);
            $updTenderDetail3 = Tenderdetail::where(['tenderId'=>$request->tenderId,'penawaranStatus'=>'1'])
                  ->update([
                      'penawaranStatus' => '6',
                ]);
            
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data gagal disimpan !'], 200);
        }
    }
    
    public function pemenang(Request $request)
    {         
        $tenderDetail = Tenderdetail::with(['tender','supplier'])
                  ->where(['tenderId'=>$request->tenderId, 'penawaranStatus'=>'3'])
                  ->where('penawaranNego', '>', '0')
                  ->get();   
        
        return ['tenderDetail'=>$tenderDetail];
    }
    
    public function pemenangpost(Request $request)
    {        
        $update = Tender::where('tenderId', $request->tenderId)
              ->update([
                  'tenderReff1' => $request->tenderRemark1,
                  'tenderReff2' => $request->tenderRemark2,
                  'tenderStatus' => '4',
            ]);        
        
        if($update){      
            $tenderDetail = Tenderdetail::with(['tender','supplier'])
                    ->where(['tenderId'=>$request->tenderId, 'tenderPenawaranId'=>$request->tenderPenawaranId])->get();  
            
            if($tenderDetail[0]['tender'][0]['tenderType']=='1'){
                $typeTender = "Tender";
            }elseif($tenderDetail[0]['tender'][0]['tenderType']=='2'){
                $typeTender = "Non Tender";            
            }else{
                $typeTender = "Repead Orders";            
            }

            /** Send Email **/
            $docType = $tenderDetail[0]['tender'][0]['tenderReff2']!="NULL" ? "(".$tenderDetail[0]['tender'][0]['tenderReff2'].")" : "";
            $isiEmail="<html>";
            $isiEmail.="<html>";
            $isiEmail.="<body>";
            $isiEmail.="Selamat... <br />";
            $isiEmail.="Bersama ini kami informasikan kepada Bapak/Ibu bahwa perusaan Bapak/Ibu memenangkan kegiatan pengadaan di PT Semen Indoensia Logistik Group dengan rincian kegiatan sebagai berikut: <br />";
            $isiEmail.="<table style='border:0;bordercolor=#ffffff' width='100%'>";
            $isiEmail.="<tr>";
            $isiEmail.="<td width='40'>Nomer</td>";
            $isiEmail.="<td width='10'>:</td>";
            $isiEmail.="<td>".$tenderDetail[0]['tender'][0]['tenderNomor']."</td>";
            $isiEmail.="</tr>";
            $isiEmail.="<tr>";
            $isiEmail.="<td>Deskripsi</td>";
            $isiEmail.="<td>:</td>";
            $isiEmail.="<td>".$tenderDetail[0]['tender'][0]['tenderNama']."</td>";
            $isiEmail.="</tr>";                   
            $isiEmail.="<tr>";
            $isiEmail.="<td>Type Kegiatan</td>";
            $isiEmail.="<td>:</td>";
            $isiEmail.="<td>".$typeTender."</td>";
            $isiEmail.="<td>Nomer OP / OJ</td>";
            $isiEmail.="<td>:</td>";
            $isiEmail.="<td>".$tenderDetail[0]['tender'][0]['tenderReff1']." ".$docType."</td>";
            $isiEmail.="</tr>"; 
            $isiEmail.="<tr>";
            $isiEmail.="<td>Harga</td>";
            $isiEmail.="<td>:</td>";
            $isiEmail.="<td>".$tenderDetail[0]['penawaranNego']."</td>";
            $isiEmail.="<tr>";
            $isiEmail.="</table><br />";
            $isiEmail.="Silakan cetak bukti pemenangan di eproc.silog.co.id dan gunakan user dan password anda untuk login ke aplikasi. <br />";
            $isiEmail.="Untuk informasi lebih lanjut, silakan kirim email ke pengadaan@silog.co.id<br /><br />";
            $isiEmail.="Terima Kasih <br /><br /><br />";
            $isiEmail.="Bagian Pengadaan <br />";
            $isiEmail.="Semen Indonesia Logistik Group <br />";
            $isiEmail.="<h5>Mohon untuk tidak membalas karena email ini dikirimkan secara otomatis oleh sistem</h5>";
            $isiEmail.= "</body>";
            $isiEmail.="</html>"; 

            $email = new Sendmail;
            $email->Sender = "sistem@silog.co.id";
            $email->Tanggal = date("Y-m-d H:i:s");
            $email->Recipients = $tenderDetail[0]['supplier'][0]['supEmail'];
            $email->CC = ""; 
            $email->SubjectEmail = "Informasi Pemenang Kegiatan Pengadaan dari Semen Indonesia Logistik Group";
            $email->IsiEmail = addslashes($isiEmail);
            $email->Stat = "outbox";
            $email->email_password = "Veteran1974!@Gs";
            $email->ContentEmail = "0";
            $email->sistem = "eprocUndanganNego";
            $email->refId = $tenderDetail[0]['tenderPenawaranId'];
            $email->save();

            $updTenderDetail = Tenderdetail::where(['tenderId'=>$request->tenderId, 'tenderPenawaranId'=>$request->tenderPenawaranId])
                                ->update([
                                    'penawaranStatus' => '4',
                              ]);
                    
            $updTenderDetail2 = Tenderdetail::where(['tenderId'=>$request->tenderId,'penawaranStatus'=>'3'])
                  ->update([
                      'penawaranStatus' => '5',
                ]);
            
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data gagal disimpan !'], 200);
        }
    }
}
