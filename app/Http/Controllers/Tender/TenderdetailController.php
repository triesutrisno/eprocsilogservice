<?php

namespace App\Http\Controllers\Tender;

use App\Http\Controllers\Controller;
use App\Http\Models\Tender\Tenderdetail;
use Illuminate\Http\Request;
use App\Http\Models\Supplier\Supplier;
use App\Http\Models\Tender\Tender;
use App\Http\Models\Currency\Currency;

class TenderdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $supp = Supplier::where(['supEmail'=>$request->email,'supStatus'=>'3'])->get();
        //return $supp;

        $tenderDetail = Tenderdetail::with(['tender','supplier'])
            ->where(['supId'=>$supp[0]['supId'],'penawaranStatus'=>'1'])->get();

        $dtPengikut = Tenderdetail::with(['tender','supplier'])
            ->where(['supId'=>$supp[0]['supId'],'penawaranStatus'=>'2'])->get();

        $dtNego = Tenderdetail::with(['tender','supplier'])
            ->where(['supId'=>$supp[0]['supId'],'penawaranStatus'=>'3'])->get();
        
        $dtPemenang = Tenderdetail::with(['tender','supplier'])                
            ->where(['supId'=>$supp[0]['supId'],'penawaranStatus'=>'4'])->get();
        
        $dtKalah = Tenderdetail::with(['tender','supplier'])                
            ->where(['supId'=>$supp[0]['supId'],'penawaranStatus'=>'5'])->get();
        
        return ['tenderDetail'=>$tenderDetail, 'dtPengikut'=>$dtPengikut, 'dtNego'=>$dtNego, 'dtPemenang'=>$dtPemenang, 'dtKalah'=>$dtKalah];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Tender\Tenderdetail  $tenderdetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $tenderDetail = Tenderdetail::with(['tender','supplier', 'email'])
                  ->where(['tenderPenawaranId'=>$id])->get();   
        
        $tender = Tender::with(['kategori', 'supplier'])
                  ->where(['tenderId'=>$tenderDetail['0']['tenderId']])->get();

        return ['tenderDetail'=>$tenderDetail, 'tender'=>$tender];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Tender\Tenderdetail  $tenderdetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $curr = Currency::all();  

        $tenderDetail = Tenderdetail::with(['tender','supplier'])
                  ->where(['tenderPenawaranId'=>$id])->get(); 
        
        return ['tenderDetail'=>$tenderDetail, 'curr'=>$curr];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Tender\Tenderdetail  $tenderdetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tenderdetail $tenderdetail)
    {
        $tenderDetail = Tenderdetail::where(['tenderPenawaranId'=>$request->tenderPenawaranId])->get();
        $tenderDetailFile = $request->penawaranFile != NULL ? $request->penawaranFile : $tenderDetail[0]['penawaranFile'];
        $tenderDetailGambar = $request->penawaranGambar != NULL ? $request->penawaranGambar : $tenderDetail[0]['penawaranGambar'];

        $upd = Tenderdetail::where('tenderPenawaranId', $request->tenderPenawaranId)
              ->update([
                  'penawaranCurency' => $request->penawaranCurency,
                  'penawaranHarga' => $request->penawaranHarga,
                  'penawaranExpired' => $request->penawaranExpired,
                  'penawaranSpesifikasi' => $request->penawaranSpesifikasi,
                  'penawaranFile' => $tenderDetailFile,
                  'penawaranGambar' => $tenderDetailGambar,
                  'penawaranStatus' => 2,
        ]);
        if($upd)
        {
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data gagal disimpan !'], 200);
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Tender\Tenderdetail  $tenderdetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tenderdetail $tenderdetail)
    {
        //
    }
    
    public function nego($id)
    {
        $tenderDetail = Tenderdetail::with(['tender','supplier'])
                  ->where(['tenderPenawaranId'=>$id])->get(); 
        
        return ['tenderDetail'=>$tenderDetail];
    }
    
    public function negopost(Request $request, Tenderdetail $tenderdetail)
    {
        $upd = Tenderdetail::where('tenderPenawaranId', $request->tenderPenawaranId)
              ->update([
                  'penawaranNego' => $request->penawaranNego,
                  'tglNego' => $request->tglNego,
        ]);
        
        if($upd)
        {
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data gagal disimpan !'], 200);
        }
        
    }   
    
    public function cetaktender(Request $request)
    {
        $cetak = Tenderdetail::ambildata2('20000861','OP','10100');
        return $cetak;
    }
}
