<?php

namespace App\Http\Controllers\Unit;

use App\Http\Controllers\Controller;
use App\Http\Models\Unit\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unit = Unit::all();
        return $unit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Unit::where(['comp'=>$request->comp, 'kode'=>$request->kode, 'status'=>'1', 'deleted_at'=>NULL])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Unit::create($request->all());
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Unit\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Unit\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $id)
    {
        $unit = Unit::where(['id'=>$id])->get();
        return $unit;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Unit\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $id)
    {
        if (Unit::where([
                ['comp', '=', $request->comp],
                ['kode', '=', $request->kode],
                ['status', '=', '1'],
                ['deleted_at', '=', 'NULL'],
                ['unitId', '!=', $id]
        ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Unit::where('unitId', $id)
              ->update([
                  'comp' => $request->comp,
                  'kode' => $request->kode,
                  'unit' => $request->unit,
                  'keterangan' => $request->keterangan,
                  'status' => $request->status,
            ]);
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil diubah !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Unit\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $id)
    {
        $del = Unit::destroy($id);
        if ($del){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil dihapus !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa dihapus !'], 200);
        }
    }
}
