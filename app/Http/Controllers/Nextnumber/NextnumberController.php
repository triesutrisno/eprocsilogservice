<?php

namespace App\Http\Controllers\Nextnumber;

use App\Http\Controllers\Controller;
use App\Http\Models\Nextnumber\Nextnumber;
use Illuminate\Http\Request;

class NextnumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nextNumber = Nextnumber::all();
        return $nextNumber;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Nextnumber::where([
                'comp'=>$request->comp, 
                'tahun'=>$request->tahun, 
                'kode'=>$request->kode, 
                'jenis'=>$request->jenis, 
                'status'=>'1'
            ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Nextnumber::create($request->all());
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil disimpan !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Nextnumber\Nextnumber  $nextnumber
     * @return \Illuminate\Http\Response
     */
    public function show(Nextnumber $nextnumber)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Nextnumber\Nextnumber  $nextnumber
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nextNumber = Nextnumber::where(['id'=>$id])->get();
        return $nextNumber;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Nextnumber\Nextnumber  $nextnumber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nextnumber $nextnumber)
    {
        if (Nextnumber::where([
                ['comp', '=', $request->comp],
                ['tahun', '=', $request->tahun],
                ['kode', '=', $request->kode],
                ['jenis', '=', $request->jenis],
                ['status', '=', '1'],
                ['id', '!=', $request->id]
        ])->doesntExist()) { // Cek data apakah sudah ada atau belum di database            
            Nextnumber::where('id', $request->id)
              ->update([
                  'comp' => $request->comp,
                  'tahun' => $request->tahun,
                  'kode' => $request->kode,
                  'jenis' => $request->jenis,
                  'keterangan' => $request->keterangan,
                  'urutan' => $request->urutan,
                  'status' => $request->status,
            ]);
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil diubah !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data sudah ada !'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Nextnumber\Nextnumber  $nextnumber
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Nextnumber::destroy($id);
        if ($del){
            return response()->json(['kode'=>'99', 'pesan'=>'Data berhasil dihapus !'], 200);
        }else{
            return response()->json(['kode'=>'90', 'pesan'=>'Data tidak bisa dihapus !'], 200);
        }
    }
}
