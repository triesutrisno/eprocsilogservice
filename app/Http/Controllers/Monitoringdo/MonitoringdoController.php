<?php

namespace App\Http\Controllers\Monitoringdo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Models\Supplier\Supplier;


class MonitoringdoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kdPlant = $request->param["kdPlant"] != NULL ? $request->param["kdPlant"] : "";
        $tglA = $request->param["tglA"] != NULL ? $request->param["tglA"] : date("Y-m-d");
        $tglB = $request->param["tglB"] != NULL ? $request->param["tglB"] : date("Y-m-d");
        $nopin = $request->param["nopin"] != NULL ? $request->param["nopin"] : "";
        $noDo = $request->param["noDo"] != NULL ? $request->param["noDo"] : "";
        $status = $request->param["status"] != NULL ? $request->param["status"] : "";
        $spjexternal = $request->param["spjexternal"] != NULL ? $request->param["spjexternal"] : "";

        #return $request->post("param")["tglA"];  
        
        $dtSupp = Supplier::where(['supEmail'=>$request->param["email"]])->get();
        #return $dtSupp;

        $data = DB::connection('PRODDTA')
            ->table('VW_MONITORING')
            ->select(
                'KODEPLANT',
                'NO_DO',
                'DO_TY',
                'DO_CO',
                'DO_LINE',
                'KODEPLANT',
                'STS_PERJ',
                'NOPIN',
                'NOPOL',
                'KODEDRV',
                'NAMADRV',
                'NIKDRV',
                'TGLREADY',
                'TGLMATCH',
                'TGLMASUKMUAT',
                DB::raw("lpad(JAMMASUKMUAT, 6, '0') as JAMMASUKMUAT"),
                'TGLKELUARMUAT',
                DB::raw("lpad(JAMKELUARMUAT, 6, '0') as JAMKELUARMUAT"),
                'TGLMASUKBONGKAR',
                DB::raw("lpad(JAMMASUKBONGKAR, 6, '0') as JAMMASUKBONGKAR"),
                'TGLKELUARBONGKAR',
                DB::raw("lpad(JAMKELUARBONGKAR, 6, '0') as JAMKELUARBONGKAR"),
                'REF1',
                'KODE_ASALMUAT',
                'NAMA_ASALMUAT',
                'KODE_SHIPTO',
                'NAMA_SHIPTO',
                'KODE_VDRIVER',
                'NAMA_VDRIVER',
                'KODE_PLG',
                'NAMA_PLG',
                'TGLEPOD',
                DB::raw("lpad(JAMEPOD, 6, '0') as JAMEPOD"),
                'LOGPROGRAM',
                'TGLBRGKATMUAT',
                DB::raw("lpad(JAMBRGKATMUAT, 6, '0') as JAMBRGKATMUAT"),
                'TGLKONFMUAT',
                DB::raw("lpad(JAMKONFMUAT, 6, '0') as JAMKONFMUAT"),
                'TGLTRANSFER',
                DB::raw("lpad(JAMTRANSFER, 6, '0') as JAMTRANSFER"),
                'TGLKEMBALI',
                DB::raw("lpad(JAMKEMBALI, 6, '0') as JAMKEMBALI")
            )
            ->where(['KODE_VDRIVER'=>$dtSupp[0]['kodeJDE']])
            ->whereBetween('TGLMATCH', [$tglA, $tglB])
            ->when($kdPlant, function ($query, $kdPlant) {
                    return $query->where(DB::raw("TRIM(KODEPLANT)"), $kdPlant);
                })         
            ->when($nopin, function ($query, $nopin) {
                    return $query->where(DB::raw("TRIM(NOPIN)"), $nopin);
                })
            ->when($noDo, function ($query, $noDo) {
                    return $query->where('NO_DO', $noDo);
                })
            ->when($spjexternal, function ($query, $spjexternal) {
                    return $query->where('REF1', $spjexternal);
                })
            ->when($status, function ($query, $status) {
                    return $query->where('STS_PERJ', $status);
                })
            ->orderBy('STS_PERJ', 'asc')
            ->get();

        $datax = DB::connection('PRODDTA')
            ->table('F0101')
            ->select(
                'ABALKY',
                'ABALPH'
            )
            ->where(DB::raw("TRIM(ABAT1)"),'=','BP')
            ->where(DB::raw("TRIM(ABALKY)"), 'like', 'A%')
            ->orderBy('ABALKY', 'asc')
            ->get();

        return ['data'=>$data, 'data2'=>$datax];
    }
}
