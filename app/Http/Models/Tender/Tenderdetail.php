<?php

namespace App\Http\Models\Tender;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Tenderdetail extends Model
{
    use SoftDeletes;
    protected $table = "tb_tender_penawaran";
    protected $primaryKey = "tenderPenawaranId";
    protected $fillable = [
        'tenderId', 
        'supId', 
        'penawaranSpesifikasi',
        'penawaranCurency',
        'penawaranHarga',
        'penawaranExpired',
        'penawaranFile',
        'penawaranGambar',
        'penawaranStatus',
    ];
    
    public static function ambilData($doco,$dcto,$kcoo){
        $data = DB::connection('PRODDTA')
                ->select(
                        DB::raw("select *
                                 from f4311 a
                                 where pddoco = '$doco' 
                                      AND pddcto='$dcto'
                                      AND pdkcoo='$kcoo'")
                );
                    
        return $data;
    }
    
    public static function ambilData2($doco,$dcto,$kcoo){
        $data = DB::connection('EPROCSILOG')
                ->select(
                        DB::raw("select *
                                 from dw.dbo.fact_f4311 a 
                                 left join dw.dbo.fact_f4301 b on (a.pddoco=phdoco and a.pddcto=b.phdcto and a.pdkcoo=b.phkcoo)
                                 left join dw.dbo.dim_f0101 c on (a.pdan8=c.aban8)
                                 where pddoco = '$doco' 
                                      AND pddcto='$dcto'
                                      AND pdkcoo='$kcoo'")
                );
                    
        return $data;
    }
    
    public function tender()
    {
        return $this->hasMany('App\Http\Models\Tender\Tender','tenderId','tenderId');
    }
    
    public function supplier()
    {
        return $this->hasMany('App\Http\Models\Supplier\Supplier','supId','supId');
    }

    public function email()
    {
        return $this->hasMany('App\Http\Models\Sendmail\Sendmail','refId','tenderPenawaranId');
    }
}
