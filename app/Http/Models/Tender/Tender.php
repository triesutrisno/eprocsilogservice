<?php

namespace App\Http\Models\Tender;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tender extends Model
{
    use SoftDeletes;
    protected $table = "tb_tender";
    protected $primaryKey = "tenderId";
    protected $fillable = [
        'unitId',
        'comp',
        'tenderTahun',
        'tenderNomor',
        'tenderNama',
        'tenderJenis',
        'tenderSpesifikasi',
        'tenderCurency',
        'tenderBudget',
        'statusBudget',
        'tenderType',
        'mkatId',
        'supId',
        'tenderReff1',
        'tenderReff2',
        'tenderFile',
        'tenderGambar',
        'tglTutup',
        'tglAnwijzing',
        'lokasiAnwijzing',
        'tenderStatus',
    ];
    
    public function kategori()
    {
        return $this->hasMany('App\Http\Models\Kategori\Kategori','mkatId','mkatId');
    }
    
    public function supplier()
    {
        return $this->hasMany('App\Http\Models\Supplier\Supplier','supId','supId');
    }
    
    public function currency()
    {
        return $this->hasMany('App\Http\Models\Currency\Currency','currKode','tenderCurency');
    }
}
