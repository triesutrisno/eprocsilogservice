<?php

namespace App\Http\Models\Permintaanpembayaran;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permintaanpembayarandetail extends Model
{
    use SoftDeletes;
    protected $table = "permintaan_pembayaran_detail";
    protected $primaryKey = "ppDetailId";
    protected $fillable = [
        'ppId',
        'noDokumen',
        'typeDokumen',
        'lineDokumen',
        'itemNumber',
        'itemName',
        'curency',
        'qty',
        'satuan',
        'totalNilai',
        'openNilai',
        'requestNilai',
        'noPP',
        'typePP',
        'userEmail',
        'status',
    ];
}
