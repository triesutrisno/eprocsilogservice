<?php

namespace App\Http\Models\Permintaanpembayaran;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permintaanpembayaran extends Model
{
    use SoftDeletes;
    protected $table = "permintaan_pembayaran";
    protected $primaryKey = "ppId";
    protected $fillable = [
        'supId',
        'comp',
        'noDokumen',
        'typeDokumen',
        'keterangan',
        'noInvoice',
        'status',
        'tglVerifikasi',
        'ppFile',
        'remark',
        'noPP',
        'typeDokPP',
        'noEPP',
        'noBuktiPembayaran',
        'userEmail',
    ];
    
    public function ppdetail()
    {
        return $this->hasMany('App\Http\Models\Permintaanpembayaran\Permintaanpembayarandetail','ppId','ppId');
    }
    
    public function supplier()
    {
        return $this->hasMany('App\Http\Models\Supplier\Supplier','supId','supId');
    }
}
