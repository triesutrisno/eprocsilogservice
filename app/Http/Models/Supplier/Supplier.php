<?php

namespace App\Http\Models\Supplier;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;
    protected $table = "m_supplier";
    protected $primaryKey = "supId";
    protected $fillable = [
        'supNama',
        'supWebsite',
        'supEmail',
        'supEmailStatus',
        'supTlp',
        'supAlamat',
        'mpropId',
        'mkabId',
        'mkecId',
        'mkatId',
        'mkualId',
        'supSIUP',
        'supSIUPFile',
        'supNPWP',
        'supNPWPFile',
        'supNPPKP',
        'supNPPKPFile',
        'supTDP',
        'supTDPFile',
        'supAkteNoteris',
        'supDomisili',
        'supBank1',
        'supNorek1',
        'supBank1File',
        'supBank2',
        'supNorek2',
        'supBank2File',
        'pemimpinNama',
        'pemimpinJabatan',
        'pemimpinKTP',
        'pemimpinKTPFile',
        'pemimpinEmail',
        'pemimpinTlp',
        'pemimpinHp',
        'pemimpinAlamat',
        'PemilikNama',
        'PemilikJabatan',
        'PemilikKTP',
        'PemilikKTPFile',
        'PemilikEmail',
        'PemilikTlp',
        'PemilikHp',
        'PemilikAlamat',            
        'supSetuju',          
        'supStatus',
        'has',
        'pesan',
        'paktaIntegritas',
    ];   
    
    public function propinsi()
    {
        return $this->hasMany('App\Http\Models\Propinsi\Propinsi','mpropId','mpropId');
    }
    
    public function kabupaten()
    {
        return $this->hasMany('App\Http\Models\Kabupaten\Kabupaten','mkabId','mkabId');
    }
    
    public function kecamatan()
    {
        return $this->hasMany('App\Http\Models\Kecamatan\Kecamatan','mkecId','mkecId');
    }
    
    public function kategori()
    {
        return $this->hasMany('App\Http\Models\Kategori\Kategori','mkatId','mkatId');
    }
    
    public function kualifikasi()
    {
        return $this->hasMany('App\Http\Models\Kualifikasi\Kualifikasi','mkualId','mkualId');
    }
    
    public function pegawai()
    {
        return $this->hasMany('App\Http\Models\Pegawai\Pegawai','NO_KTP','PemilikKTP');
    }
}
