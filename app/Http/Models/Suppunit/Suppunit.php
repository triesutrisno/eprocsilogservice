<?php

namespace App\Http\Models\Suppunit;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suppunit extends Model
{
    use SoftDeletes;
    protected $table = "suppunit";
    protected $primaryKey = "suppunitId";
    protected $fillable = ['suppId','unitId'];
}
