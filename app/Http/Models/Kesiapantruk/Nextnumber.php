<?php

namespace App\Http\Models\Kesiapantruk;

use Illuminate\Database\Eloquent\Model;

class Nextnumber extends Model
{    
    protected $connection = 'BEGONIADB';
    protected $table = "tms_nextnumber";
    protected $primaryKey = "numberId";
    protected $fillable = [
        'nextnumber',
        'docType',
        'keterangan',
        'status'
    ];
    
}
