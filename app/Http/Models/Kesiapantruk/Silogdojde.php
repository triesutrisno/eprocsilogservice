<?php

namespace App\Http\Models\Kesiapantruk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Silogdojde extends Model
{
    #use SoftDeletes;
   protected $connection = 'PRODDTA';
    protected $table = 'f4211';
    protected $primaryKey = "SDDOCO";
    public $timestamps = false;
    protected $fillable = [
        'SDDOCO',
        'SDDCTO',
        'SDLNID',
        'SDKCOO',
        'SDSO14',
        'SDNXTR',
        'SDLTTR',
        'SDCARS',
        'SDMOT',
        'SDSHPN'
    ];
}
