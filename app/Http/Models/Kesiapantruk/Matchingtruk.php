<?php

namespace App\Http\Models\Kesiapantruk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Matchingtruk extends Model
{
    #use SoftDeletes;
    
    
    protected $connection = 'PRODDTA';
    protected $table = 'F5501012';
    protected $primaryKey = "SPDOCO";
    public $timestamps = false;
    protected $fillable = [
        'SPKCOO',
        'SPDCTO',
        'SPDOCO',
        'SPLNID',
        'SPTRDJ',
        'SPDRQJ',
        'SPSHAN',
        'SPEMCU',
        'SPITM',
        'SPLITM',
        'SPAITM',
        'SPLOCN',
        'SPUOM',
        'SPUORG',
        'SPAA',
        'SPAA1',
        'SPKCO',
        'SPDCT',
        'SPDOC',
        'SPXLIN',
        'SPEV01',
        'SPEV02',
        'SPEV03',
        'SPEV04',
        'SPEV05',
        'SPEV06',
        'SPPID',
        'SPJOBN',
        'SPUSER',
        'SPUPMJ',
        'SPUPMT',
    ];
}
