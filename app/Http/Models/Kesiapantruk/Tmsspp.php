<?php

namespace App\Http\Models\Kesiapantruk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tmsspp extends Model
{
    //
    protected $connection = 'PRODDTA';
    protected $table = 'F5501011';
    protected $primaryKey = "SPDOCO";
    public $timestamps = false;
    protected $fillable = [
        'SPKCOO',
        'SPDCTO',
        'SPDOCO',
        'SPMCU',
        'SPTRDJ',
        'SPDRQJ',
        'SPAN8',
        'SPALPH',
        'SPEMCU',
        'SPCARS',
        'SPVEHI',
        'SPMOT',
        'SPSHAN',
        'SPLOCN',
        'SPUORG',
        'SPUOM',
        'SPAA',
        'SPAA1',
        'SPKCO',
        'SPDCT',
        'SPDOC',
        'SPDOCSO',
        'SPDOCSPJ',
        'SPEV01',
        'SPEV02',
        'SPEV03',
        'SPEV04',
        'SPEV05',
        'SPEV06',
        'SPPID',
        'SPJOBN',
        'SPUSER',
        'SPUPMJ',
        'SPUPMT',
        'SPUPDT'
    ];
}
