<?php

namespace App\Http\Models\Kesiapantruk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kesiapantruk extends Model
{
    #use SoftDeletes;
   protected $connection = 'PRODDTA';
    protected $table = 'F5501019';
    protected $primaryKey = "SLDOCO";
    public $timestamps = false;
    protected $fillable = [
        'SLDOCO',
        'SLDCTO',
        'SLKCOO',
        'SLVEHI',
        'SLRLNO',
        'SLORGN',
        'SLVEND',
        'SLSHAN',
        'SLANID',
        'SLSSTS',
        'SLTRDJ',
        'SLTRTM',
        'SLDPDT',
        'SLDETM',
        'SLLDDT',
        'SLLDTM',
        'SLDLDT',
        'SLDLTM',
        'SLADDJ',
        'SLADTM',
        'SLRMK',
        'SLCDAA',
        'SLCDBB',
        'SLCDCC',
        'SLUSAP',
        'SLAPPJ',
        'SLATIM',
        'SLAA1',
        'SLAA2',
        'SLAA3',
        'SLTORG',
        'SLCRDJ',
        'SLCRTM',
        'SLUSER',
        'SLUPMJ',
        'SLUPMT',
        'SLJOBN',
        'SLPID',
        'SLURCD',
        'SLURAT',
        'SLURRF',
        'SLURDT',
        'SLURAB',
        'SLCARS',
    ];
}
