<?php

namespace App\Http\Models;

use App\Http\Models\Supplier\Supplier;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BeritaAcaraSBI extends Model
{
    use SoftDeletes;
    protected $table = "berita_acara_sbi";
    protected $primaryKey = "id_berita_acara";

    public function spj()
    {
        return $this->hasMany(BeritaAcaraSpjSBI::class, 'id_berita_acara', 'id_berita_acara');
    }

    public function spj_approved()
    {
        return $this->hasMany(BeritaAcaraSpjSBI::class, 'id_berita_acara', 'id_berita_acara');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function approvedByUser()
    {
        return $this->belongsTo(User::class, 'approved_by', 'id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'id_supplier', 'supId');
    }

    public function suratKlaimApprovedByUser()
    {
        return $this->belongsTo(User::class, 'surat_klaim_approved_by', 'id');
    }
}
