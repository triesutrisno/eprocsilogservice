<?php

namespace App\Http\Models;

use App\Http\Models\Supplier\Supplier;
use App\User;
use Illuminate\Database\Eloquent\Model;

class InvoiceMonitor extends Model
{
    protected $table = "invoice_monitor";
    protected $primaryKey = "id_invoice_monitor";
    protected $guarded=['id_invoice_monitor'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'kode_sap_vendor', 'kodeSAP');
    }
}
