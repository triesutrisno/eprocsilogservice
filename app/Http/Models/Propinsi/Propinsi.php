<?php

namespace App\Http\Models\Propinsi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Propinsi extends Model
{
    use SoftDeletes;
    protected $table = "m_propinsi";
    protected $primaryKey = "mpropId";
    protected $fillable = ['mpropNama','mpropStatus'];
}
