<?php

namespace App\Http\Models\Kecamatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kecamatan extends Model
{
    use SoftDeletes;
    protected $table = "m_kecamatan";
    protected $primaryKey = "mkecId";
    protected $fillable = ['mkecNama','mkecStatus'];
}
