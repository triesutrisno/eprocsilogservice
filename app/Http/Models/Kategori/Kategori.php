<?php

namespace App\Http\Models\Kategori;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kategori extends Model
{
    use SoftDeletes;
    protected $table = "m_kategori";
    protected $primaryKey = "mkatId";
    protected $fillable = ['mkatNama','mkatStatus'];
}
