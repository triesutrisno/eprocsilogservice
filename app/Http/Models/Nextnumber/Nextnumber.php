<?php

namespace App\Http\Models\Nextnumber;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nextnumber extends Model
{
    use SoftDeletes;
    protected $table = "tb_next_number";
    protected $primaryKey = "id";
    protected $fillable = [
        'comp',
        'tahun',
        'kode',
        'jenis',
        'keterangan',
        'urutan',
        'status',
    ];
}
