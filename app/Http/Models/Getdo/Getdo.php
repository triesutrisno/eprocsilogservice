<?php

namespace App\Http\Models\Getdo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Getdo extends Model
{
    #use SoftDeletes;
    protected $connection = 'PRODDTA';
    protected $table = 'VW_GETDO';
    protected $primaryKey = "NO_DO";
    public $timestamps = false;
    protected $fillable = [
        'NO_DO',
        'LINE_DO',
        'DO_TY',
        'DO_CO',
        'TGL_REQ_KIRIM',
        'KODE_VENDOR',
        'ABALPH',
        'KODE_SHIPTO',
        'NAMA_SHIPTO',
        'SHIPTO_PALET',
        'ALAMAT1_SHIPTO',
        'ALAMAT2_SHIPTO',
        'KOTA_SHIPTO',
        'KODE_MUATDI',
        'NAMA_MUATDI',
        'ALAMAT1_MUATDI',
        'ALAMAT2_MUATDI',
        'KOTA_MUATDI',
        'KODE_BRG',
        'NAMA_BRG',
        'SAT',
        'QTY',
        'MOT',
        'KET_MOT',
    ];
}
