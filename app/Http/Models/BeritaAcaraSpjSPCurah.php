<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BeritaAcaraSpjSPCurah extends Model
{
    use SoftDeletes;
    protected $table = "berita_acara_spj_sp_curah";
    protected $primaryKey = "id_berita_acara_spj";
    protected $guarded = ['id_berita_acara_spj'];

    public function beritaAcara()
    {
        return $this->belongsTo(BeritaAcaraSPCurah::class, 'id_berita_acara', 'id_berita_acara');
    }
}
