<?php

namespace App\Http\Models\Unit;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;
    protected $table = "tb_unit";
    protected $primaryKey = "unitId";
    protected $fillable = ['unit', 'comp', 'kode', 'status'];
}
