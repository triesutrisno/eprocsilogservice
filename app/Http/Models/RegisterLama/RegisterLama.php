<?php

namespace App\Http\Models\Register;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Register extends Model
{
    use SoftDeletes;
    protected $table = "m_supplier";
    protected $primaryKey = "supId";
    protected $fillable = [
        'supNama',
        'supWebsite',
        'supEmail',
        'supEmailStatus',
        'supTlp',
        'supAlamat',
        'mpropId',
        'mkabId',
        'mkecId',
        'mkatId',
        'mkualId',
        'supSIUP',
        'supSIUPFile',
        'supNPWP',
        'supNPWPFile',
        'supNPPKP',
        'supNPPKPFile',
        'supTDP',
        'supTDPFile',
        'supAkteNoteris',
        'supDomisili',
        'supBank1',
        'supNorek1',
        'supBank2',
        'supNorek2',
        'pemimpinNama',
        'pemimpinJabatan',
        'pemimpinKTP',
        'pemimpinKTPFile',
        'pemimpinEmail',
        'pemimpinTlp',
        'pemimpinHp',
        'pemimpinAlamat',
        'PemilikNama',
        'PemilikJabatan',
        'PemilikKTP',
        'PemilikKTPFile',
        'PemilikEmail',
        'PemilikTlp',
        'PemilikHp',
        'PemilikAlamat',            
        'supSetuju',          
        'supStatus',
    ];
}
