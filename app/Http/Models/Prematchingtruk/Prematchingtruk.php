<?php

namespace App\Http\Models\Prematchingtruk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prematchingtruk extends Model
{
    #use SoftDeletes;
    protected $table = "tb_prematching";
    protected $primaryKey = "id";
    protected $fillable = [
        'email',
        'kdVendor',
        'nmVendor',
        'noDO',
        'lineDO',
        'typeDO',
        'compDO',
        'tglRequestKirim',
        'kdShipto',
        'nmShipto',
        'kdMuat',
        'nmMuat',
        'kdBarang',
        'nmBarang',
        'satuan',
        'qty',
        'kdMot',
        'nmMot',
    ];
}
