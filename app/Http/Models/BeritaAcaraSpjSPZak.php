<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BeritaAcaraSpjSPZak extends Model
{
    use SoftDeletes;
    protected $table = "berita_acara_spj_sp_zak";
    protected $primaryKey = "id_berita_acara_spj";
    protected $guarded = ['id_berita_acara_spj'];

    public function beritaAcara()
    {
        return $this->belongsTo(BeritaAcaraSPZak::class, 'id_berita_acara', 'id_berita_acara');
    }
}
