<?php

namespace App\Http\Models\Pegawai;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = "vwPegawai";
    protected $primaryKey = "NIK";
}
