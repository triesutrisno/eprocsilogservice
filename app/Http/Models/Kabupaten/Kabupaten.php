<?php

namespace App\Http\Models\Kabupaten;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kabupaten extends Model
{
    use SoftDeletes;
    protected $table = "m_kabupaten";
    protected $primaryKey = "mkabId";
    protected $fillable = ['mpropId','mkabNama','mkabStatus'];
}
