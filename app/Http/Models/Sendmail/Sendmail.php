<?php

namespace App\Http\Models\Sendmail;

use Illuminate\Database\Eloquent\Model;

class Sendmail extends Model
{
    protected $table = "SDM.dbo.tb_mt_email";
    protected $primaryKey = "ID";
    protected $fillable = [
        'Sender', 
        'Tanggal', 
        'Recipients', 
        'CC', 
        'SubjectEmail', 
        'IsiEmail',
        'Stat', 
        'email_password', 
        'ContentEmail', 
        'sistem', 
        'refId'
    ];
    public $timestamps = false;
}
