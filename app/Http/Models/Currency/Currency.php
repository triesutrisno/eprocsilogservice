<?php

namespace App\Http\Models\Currency;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes;
    protected $table = "m_currency";
    //protected $primaryKey = "currKode";
    protected $fillable = ['currKode','currKeterangan','currStatus'];
}
