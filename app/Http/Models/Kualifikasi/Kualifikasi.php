<?php

namespace App\Http\Models\Kualifikasi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kualifikasi extends Model
{
    protected $table = "m_kualifikasi";
    protected $primaryKey = "mkualId";
    protected $fillable = ['mkualNama','mkualStatus'];
}
