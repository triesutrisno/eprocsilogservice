<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BeritaAcaraResource extends JsonResource
{

    public function toArray($request)
    {
        $format = $this->getAttributes();
        $format['is_inputted_sap'] = $this->is_inputted_sap == 1 ? "SUDAH" : "BELUM";
        $format['periode_spj'] = $this->periode_spj_awal . " - " . $this->periode_spj_akhir;
        $format['approve_status_label'] = $this->approve_status == 1 ? "APPROVED" : ($this->approve_status == -1 ? "REJECTED" : "WAITING");
        $format['approved_by_name'] = $this->approvedByUser != null ? $this->approvedByUser->name : "";
        $format['total_spj'] = $this->spj_count;
        $format['total_approved'] = $this->spj_approved_count;
        $format['surat_klaim_approve_status_label'] = $this->surat_klaim_approve_status == 1 ? "APPROVED" : "";

        return $format;
    }
}
