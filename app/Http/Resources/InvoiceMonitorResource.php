<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceMonitorResource extends JsonResource
{

    public function toArray($request)
    {
        $format = $this->getAttributes();

        return $format;
    }
}
