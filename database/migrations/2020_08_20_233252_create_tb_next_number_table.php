<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbNextNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_next_number', function (Blueprint $table) {            
            $table->bigIncrements('id');
            $table->string('comp',10)->nullable();   
            $table->string('tahun',5)->nullable();                        
            $table->string('kode',10)->nullable();  
            $table->string('jenis',2)->nullable();         
            $table->string('keterangan',250)->nullable();  
            $table->integer('urutan')->nullable();           
            $table->string('status',2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_next_number');
    }
}
