<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbTenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_tender', function (Blueprint $table) {
            $table->bigIncrements('tenderId');
            $table->smallInteger('unitId')->nullable();
            $table->string('comp',5);
            $table->string('tenderTahun',5);
            $table->string('tenderNomor',50)->nullable();
            $table->string('tenderNama',150);
            $table->string('tenderJenis',2)->nullable();
            $table->string('tenderSpesifikasi',1000)->nullable();
            $table->string('tenderCurency',5)->nullable();
            $table->decimal('tenderBudget',15,2)->nullable();
            $table->string('statusBudget',2)->nullable();
            $table->string('tenderType',2)->nullable();
            $table->smallInteger('mkatId')->nullable();
            $table->smallInteger('supId')->nullable();
            $table->string('tenderReff1',30)->nullable();
            $table->string('tenderReff2',3)->nullable();
            $table->string('tenderFile',150)->nullable();
            $table->string('tenderGambar',150)->nullable();
            $table->date('tglTutup')->nullable(); // tgl tutup penawaran
            $table->date('tglAnwijzing')->nullable();
            $table->date('tglNegoAwal')->nullable();
            $table->date('tglNegoAkhir')->nullable();
            $table->string('lokasiAnwijzing',100)->nullable();
            $table->string('tenderStatus',2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_tender');
    }
}
