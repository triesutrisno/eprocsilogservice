<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPembayaranDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pembayaran_detail', function (Blueprint $table) {
            $table->bigIncrements('ppDetailId');
            $table->smallInteger('ppId')->nullable();
            $table->string('noDokumen',10)->nullable();                        
            $table->string('typeDokumen',3)->nullable();                        
            $table->string('lineDokumen',3)->nullable();                         
            $table->string('itemNumber',10)->nullable();                          
            $table->string('itemName',110)->nullable(); 
            $table->string('curency',5)->nullable();
            $table->decimal('totalNilai',15,2)->nullable(); 
            $table->decimal('openNilai',15,2)->nullable(); 
            $table->decimal('requestNilai',15,2)->nullable(); 
            $table->string('noPP',10)->nullable();                        
            $table->string('typePP',3)->nullable(); 
            $table->string('status',2)->nullable(); 
            $table->string('userEmail',100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pembayaran_detail');
    }
}
