<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_supplier', function (Blueprint $table) {
            $table->bigIncrements('supId');
            $table->string('supNama',100);
            $table->string('supWebsite',50)->nullable();
            $table->string('supEmail',100)->unique();
            $table->string('supEmailStatus',2)->nullable();
            $table->string('supTlp',20)->nullable();
            $table->string('supAlamat',150)->nullable();
            $table->smallInteger('mpropId')->nullable();
            $table->smallInteger('mkabId')->nullable();
            $table->smallInteger('mkecId')->nullable();
            $table->smallInteger('mkatId')->nullable();
            $table->smallInteger('mkualId')->nullable();
            $table->string('supSIUP',100)->nullable();
            $table->string('supSIUPFile',150)->nullable();
            $table->string('supNPWP',100)->nullable();
            $table->string('supNPWPFile',150)->nullable();
            $table->string('supNPPKP',100)->nullable();
            $table->string('supNPPKPFile',150)->nullable();
            $table->string('supTDP',100)->nullable();
            $table->string('supTDPFile',150)->nullable();
            $table->string('supAkteNoteris',100)->nullable();
            $table->string('supDomisili',2)->nullable();
            $table->string('supBank1',100)->nullable();
            $table->string('supNorek1',50)->nullable();
            $table->string('supBank2',100)->nullable();
            $table->string('supNorek2',50)->nullable();
            $table->string('pemimpinNama',100)->nullable();
            $table->string('pemimpinJabatan',100)->nullable();
            $table->string('pemimpinKTP',50)->nullable();
            $table->string('pemimpinKTPFile',150)->nullable();
            $table->string('pemimpinEmail',100)->nullable();
            $table->string('pemimpinTlp',20)->nullable();
            $table->string('pemimpinHp',20)->nullable();
            $table->string('pemimpinAlamat',150)->nullable();
            $table->string('PemilikNama',100)->nullable();
            $table->string('PemilikJabatan',100)->nullable();
            $table->string('PemilikKTP',50)->nullable();
            $table->string('PemilikKTPFile',150)->nullable();
            $table->string('PemilikEmail',100)->nullable();
            $table->string('PemilikTlp',20)->nullable();
            $table->string('PemilikHp',20)->nullable();
            $table->string('PemilikAlamat',150)->nullable();            
            $table->string('supSetuju',2)->nullable();
            $table->string('supStatus',2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_supplier');
    }
}
