<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pembayaran', function (Blueprint $table) {
            $table->bigIncrements('ppId');
            $table->smallInteger('supId')->nullable();
            $table->string('comp',10)->nullable();   
            $table->string('noDokumen',10)->nullable();                        
            $table->string('typeDokumen',3)->nullable();                        
            $table->string('keterangan',500)->nullable();                       
            $table->string('noInvoice',100)->nullable(); 
            $table->string('status',2)->nullable(); 
            $table->date('tglVerifikasi')->nullable();
            $table->string('ppFile',150)->nullable();
            $table->string('remark',550)->nullable();
            $table->string('noPP',20)->nullable();
            $table->string('typeDokPP',5)->nullable();
            $table->string('noEPP',20)->nullable();
            $table->string('noBuktiPembayaran',20)->nullable();
            $table->string('userEmail',100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pembayaran');
    }
}
