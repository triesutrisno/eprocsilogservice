<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbTenderPenawaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_tender_penawaran', function (Blueprint $table) {
            $table->bigIncrements('tenderPenawaranId');
            $table->smallInteger('tenderId')->nullable();
            $table->smallInteger('supId');
            $table->string('penawaranSpesifikasi',1000)->nullable();
            $table->string('penawaranCurency',1000)->nullable();
            $table->decimal('penawaranHarga',15,2)->nullable();
            $table->decimal('penawaranNego',15,2)->nullable();
            $table->date('tglNego')->nullable();
            $table->date('penawaranExpired')->nullable();
            $table->string('penawaranFile',150)->nullable();
            $table->string('penawaranGambar',150)->nullable();
            $table->string('penawaranStatus',2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_tender_penawaran');
    }
}
